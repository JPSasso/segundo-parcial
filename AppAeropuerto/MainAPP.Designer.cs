﻿namespace AppAeropuerto
{
    partial class MainAPP
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainAPP));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPasajes = new System.Windows.Forms.TabPage();
            this.cancelPasajeButton = new System.Windows.Forms.Button();
            this.dataGridViewPasajes = new System.Windows.Forms.DataGridView();
            this.buttonshowCiudadDestino = new System.Windows.Forms.Button();
            this.buttonshowCiudadOrigen = new System.Windows.Forms.Button();
            this.buttonconfirmarPasaje = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelErrorEleccionDiudades = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridViewPasajero = new System.Windows.Forms.DataGridView();
            this.clockCU5 = new AppAeropuerto.ClockCU();
            this.FechaDestTxt = new System.Windows.Forms.TextBox();
            this.FechaOrigenTxt = new System.Windows.Forms.TextBox();
            this.radioButtonSI = new System.Windows.Forms.RadioButton();
            this.radioButtonIyV = new System.Windows.Forms.RadioButton();
            this.textBoxCDest = new System.Windows.Forms.TextBox();
            this.textBoxCOrig = new System.Windows.Forms.TextBox();
            this.monthCalendarOrig = new System.Windows.Forms.MonthCalendar();
            this.monthCalendarDest = new System.Windows.Forms.MonthCalendar();
            this.comboBoxCDest = new System.Windows.Forms.ComboBox();
            this.comboBoxCOrig = new System.Windows.Forms.ComboBox();
            this.dataGridViewAvion = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPasajero = new System.Windows.Forms.TextBox();
            this.comboBoxDnis = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxAviones = new System.Windows.Forms.ComboBox();
            this.clockCU1 = new AppAeropuerto.ClockCU();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPasajeros = new System.Windows.Forms.TabPage();
            this.textBoxlastname = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxnamepila = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxdni = new System.Windows.Forms.TextBox();
            this.Precio = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.clockCU2 = new AppAeropuerto.ClockCU();
            this.tabAviones = new System.Windows.Forms.TabPage();
            this.OcupCounter = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.textBoxplanecap = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxplanemodel = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxplanepatente = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.clockCU6 = new AppAeropuerto.ClockCU();
            this.clockCU3 = new AppAeropuerto.ClockCU();
            this.tabDestinos = new System.Windows.Forms.TabPage();
            this.viewcityButton = new System.Windows.Forms.Button();
            this.textBoxLong = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxLat = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxcityname = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.textBoxcountryname = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.clockCU7 = new AppAeropuerto.ClockCU();
            this.clockCU4 = new AppAeropuerto.ClockCU();
            this.tabAnalitycs = new System.Windows.Forms.TabPage();
            this.buttonDepu = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBoxdestino = new System.Windows.Forms.TextBox();
            this.comboBoxdestinos = new System.Windows.Forms.ComboBox();
            this.radioButtonnternacinoal = new System.Windows.Forms.RadioButton();
            this.radioButtonNacinoal = new System.Windows.Forms.RadioButton();
            this.CommitQuery = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.clockCU8 = new AppAeropuerto.ClockCU();
            this.clock1 = new AppAeropuerto.ClockCU();
            this.tabControl.SuspendLayout();
            this.tabPasajes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPasajes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPasajero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAvion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPasajeros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabAviones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabDestinos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.tabAnalitycs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPasajes);
            this.tabControl.Controls.Add(this.tabPasajeros);
            this.tabControl.Controls.Add(this.tabAviones);
            this.tabControl.Controls.Add(this.tabDestinos);
            this.tabControl.Controls.Add(this.tabAnalitycs);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ItemSize = new System.Drawing.Size(100, 30);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Drawing.Point(0, 0);
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1211, 627);
            this.tabControl.TabIndex = 0;
            // 
            // tabPasajes
            // 
            this.tabPasajes.BackColor = System.Drawing.Color.Orange;
            this.tabPasajes.Controls.Add(this.cancelPasajeButton);
            this.tabPasajes.Controls.Add(this.dataGridViewPasajes);
            this.tabPasajes.Controls.Add(this.buttonshowCiudadDestino);
            this.tabPasajes.Controls.Add(this.buttonshowCiudadOrigen);
            this.tabPasajes.Controls.Add(this.buttonconfirmarPasaje);
            this.tabPasajes.Controls.Add(this.label12);
            this.tabPasajes.Controls.Add(this.label10);
            this.tabPasajes.Controls.Add(this.labelErrorEleccionDiudades);
            this.tabPasajes.Controls.Add(this.label9);
            this.tabPasajes.Controls.Add(this.dataGridViewPasajero);
            this.tabPasajes.Controls.Add(this.clockCU5);
            this.tabPasajes.Controls.Add(this.FechaDestTxt);
            this.tabPasajes.Controls.Add(this.FechaOrigenTxt);
            this.tabPasajes.Controls.Add(this.radioButtonSI);
            this.tabPasajes.Controls.Add(this.radioButtonIyV);
            this.tabPasajes.Controls.Add(this.textBoxCDest);
            this.tabPasajes.Controls.Add(this.textBoxCOrig);
            this.tabPasajes.Controls.Add(this.monthCalendarOrig);
            this.tabPasajes.Controls.Add(this.monthCalendarDest);
            this.tabPasajes.Controls.Add(this.comboBoxCDest);
            this.tabPasajes.Controls.Add(this.comboBoxCOrig);
            this.tabPasajes.Controls.Add(this.dataGridViewAvion);
            this.tabPasajes.Controls.Add(this.label11);
            this.tabPasajes.Controls.Add(this.label8);
            this.tabPasajes.Controls.Add(this.textBoxPasajero);
            this.tabPasajes.Controls.Add(this.comboBoxDnis);
            this.tabPasajes.Controls.Add(this.label7);
            this.tabPasajes.Controls.Add(this.label6);
            this.tabPasajes.Controls.Add(this.comboBoxAviones);
            this.tabPasajes.Controls.Add(this.clockCU1);
            this.tabPasajes.Controls.Add(this.label1);
            this.tabPasajes.Controls.Add(this.pictureBox1);
            this.tabPasajes.Location = new System.Drawing.Point(4, 34);
            this.tabPasajes.Name = "tabPasajes";
            this.tabPasajes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPasajes.Size = new System.Drawing.Size(1203, 589);
            this.tabPasajes.TabIndex = 4;
            this.tabPasajes.Text = "Alta de Pasajes";
            this.tabPasajes.Click += new System.EventHandler(this.tabPasajes_Click);
            // 
            // cancelPasajeButton
            // 
            this.cancelPasajeButton.BackColor = System.Drawing.Color.DarkOrange;
            this.cancelPasajeButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelPasajeButton.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.cancelPasajeButton.Location = new System.Drawing.Point(1031, 88);
            this.cancelPasajeButton.Name = "cancelPasajeButton";
            this.cancelPasajeButton.Size = new System.Drawing.Size(160, 38);
            this.cancelPasajeButton.TabIndex = 37;
            this.cancelPasajeButton.Text = "Cancelar Pasaje";
            this.cancelPasajeButton.UseVisualStyleBackColor = false;
            this.cancelPasajeButton.Click += new System.EventHandler(this.cancelPasajeButton_Click);
            // 
            // dataGridViewPasajes
            // 
            this.dataGridViewPasajes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPasajes.BackgroundColor = System.Drawing.Color.DarkOrange;
            this.dataGridViewPasajes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPasajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPasajes.Location = new System.Drawing.Point(769, 132);
            this.dataGridViewPasajes.Name = "dataGridViewPasajes";
            this.dataGridViewPasajes.Size = new System.Drawing.Size(422, 442);
            this.dataGridViewPasajes.TabIndex = 36;
            // 
            // buttonshowCiudadDestino
            // 
            this.buttonshowCiudadDestino.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonshowCiudadDestino.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonshowCiudadDestino.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonshowCiudadDestino.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.buttonshowCiudadDestino.Location = new System.Drawing.Point(615, 294);
            this.buttonshowCiudadDestino.Name = "buttonshowCiudadDestino";
            this.buttonshowCiudadDestino.Size = new System.Drawing.Size(82, 27);
            this.buttonshowCiudadDestino.TabIndex = 35;
            this.buttonshowCiudadDestino.Text = "Ver Mapa";
            this.buttonshowCiudadDestino.UseVisualStyleBackColor = false;
            this.buttonshowCiudadDestino.Click += new System.EventHandler(this.buttonshowCiudadDestino_Click);
            // 
            // buttonshowCiudadOrigen
            // 
            this.buttonshowCiudadOrigen.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonshowCiudadOrigen.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonshowCiudadOrigen.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonshowCiudadOrigen.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.buttonshowCiudadOrigen.Location = new System.Drawing.Point(268, 294);
            this.buttonshowCiudadOrigen.Name = "buttonshowCiudadOrigen";
            this.buttonshowCiudadOrigen.Size = new System.Drawing.Size(82, 27);
            this.buttonshowCiudadOrigen.TabIndex = 34;
            this.buttonshowCiudadOrigen.Text = "Ver Mapa";
            this.buttonshowCiudadOrigen.UseVisualStyleBackColor = false;
            this.buttonshowCiudadOrigen.Click += new System.EventHandler(this.buttonshowCiudadOrigen_Click);
            // 
            // buttonconfirmarPasaje
            // 
            this.buttonconfirmarPasaje.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonconfirmarPasaje.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonconfirmarPasaje.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.buttonconfirmarPasaje.Location = new System.Drawing.Point(769, 88);
            this.buttonconfirmarPasaje.Name = "buttonconfirmarPasaje";
            this.buttonconfirmarPasaje.Size = new System.Drawing.Size(160, 38);
            this.buttonconfirmarPasaje.TabIndex = 32;
            this.buttonconfirmarPasaje.Text = "Confirmar Pasaje";
            this.buttonconfirmarPasaje.UseVisualStyleBackColor = false;
            this.buttonconfirmarPasaje.Click += new System.EventHandler(this.buttonconfirmarPasaje_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(356, 324);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 18);
            this.label12.TabIndex = 31;
            this.label12.Text = "Fecha Vuelta";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(9, 324);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 18);
            this.label10.TabIndex = 30;
            this.label10.Text = "Fecha Ida";
            // 
            // labelErrorEleccionDiudades
            // 
            this.labelErrorEleccionDiudades.AutoSize = true;
            this.labelErrorEleccionDiudades.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErrorEleccionDiudades.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelErrorEleccionDiudades.Location = new System.Drawing.Point(356, 328);
            this.labelErrorEleccionDiudades.Name = "labelErrorEleccionDiudades";
            this.labelErrorEleccionDiudades.Size = new System.Drawing.Size(334, 16);
            this.labelErrorEleccionDiudades.TabIndex = 29;
            this.labelErrorEleccionDiudades.Text = "Las ciudad de destino no puede ser igual a la de Origen";
            this.labelErrorEleccionDiudades.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(218, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 18);
            this.label9.TabIndex = 28;
            this.label9.Text = "Pasajero";
            // 
            // dataGridViewPasajero
            // 
            this.dataGridViewPasajero.BackgroundColor = System.Drawing.Color.Orange;
            this.dataGridViewPasajero.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPasajero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPasajero.Location = new System.Drawing.Point(221, 53);
            this.dataGridViewPasajero.Name = "dataGridViewPasajero";
            this.dataGridViewPasajero.Size = new System.Drawing.Size(450, 73);
            this.dataGridViewPasajero.TabIndex = 27;
            // 
            // clockCU5
            // 
            this.clockCU5.BackColor = System.Drawing.Color.Transparent;
            this.clockCU5.Location = new System.Drawing.Point(1107, 25);
            this.clockCU5.Name = "clockCU5";
            this.clockCU5.Size = new System.Drawing.Size(84, 26);
            this.clockCU5.TabIndex = 26;
            // 
            // FechaDestTxt
            // 
            this.FechaDestTxt.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.FechaDestTxt.Location = new System.Drawing.Point(359, 347);
            this.FechaDestTxt.Name = "FechaDestTxt";
            this.FechaDestTxt.Size = new System.Drawing.Size(250, 27);
            this.FechaDestTxt.TabIndex = 25;
            this.FechaDestTxt.Click += new System.EventHandler(this.FechaDestTxt_Click);
            // 
            // FechaOrigenTxt
            // 
            this.FechaOrigenTxt.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.FechaOrigenTxt.Location = new System.Drawing.Point(12, 347);
            this.FechaOrigenTxt.Name = "FechaOrigenTxt";
            this.FechaOrigenTxt.Size = new System.Drawing.Size(250, 27);
            this.FechaOrigenTxt.TabIndex = 24;
            this.FechaOrigenTxt.Click += new System.EventHandler(this.textBox3_Click);
            this.FechaOrigenTxt.TextChanged += new System.EventHandler(this.FechaOrigenTxt_TextChanged);
            // 
            // radioButtonSI
            // 
            this.radioButtonSI.AutoSize = true;
            this.radioButtonSI.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.radioButtonSI.Location = new System.Drawing.Point(147, 380);
            this.radioButtonSI.Name = "radioButtonSI";
            this.radioButtonSI.Size = new System.Drawing.Size(86, 22);
            this.radioButtonSI.TabIndex = 23;
            this.radioButtonSI.Text = "Solo Ida";
            this.radioButtonSI.UseVisualStyleBackColor = true;
            // 
            // radioButtonIyV
            // 
            this.radioButtonIyV.AutoSize = true;
            this.radioButtonIyV.Checked = true;
            this.radioButtonIyV.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.radioButtonIyV.Location = new System.Drawing.Point(12, 380);
            this.radioButtonIyV.Name = "radioButtonIyV";
            this.radioButtonIyV.Size = new System.Drawing.Size(115, 22);
            this.radioButtonIyV.TabIndex = 22;
            this.radioButtonIyV.TabStop = true;
            this.radioButtonIyV.Text = "Ida y Vuelta";
            this.radioButtonIyV.UseVisualStyleBackColor = true;
            this.radioButtonIyV.CheckedChanged += new System.EventHandler(this.radioButtonIyV_CheckedChanged);
            // 
            // textBoxCDest
            // 
            this.textBoxCDest.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxCDest.Location = new System.Drawing.Point(359, 294);
            this.textBoxCDest.Name = "textBoxCDest";
            this.textBoxCDest.Size = new System.Drawing.Size(250, 27);
            this.textBoxCDest.TabIndex = 21;
            // 
            // textBoxCOrig
            // 
            this.textBoxCOrig.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxCOrig.Location = new System.Drawing.Point(12, 294);
            this.textBoxCOrig.Name = "textBoxCOrig";
            this.textBoxCOrig.Size = new System.Drawing.Size(250, 27);
            this.textBoxCOrig.TabIndex = 20;
            this.textBoxCOrig.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // monthCalendarOrig
            // 
            this.monthCalendarOrig.Location = new System.Drawing.Point(12, 412);
            this.monthCalendarOrig.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.monthCalendarOrig.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.monthCalendarOrig.Name = "monthCalendarOrig";
            this.monthCalendarOrig.TabIndex = 4;
            this.monthCalendarOrig.Visible = false;
            this.monthCalendarOrig.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarOrig_DateChanged);
            this.monthCalendarOrig.MouseLeave += new System.EventHandler(this.monthCalendarOrig_MouseLeave);
            // 
            // monthCalendarDest
            // 
            this.monthCalendarDest.Location = new System.Drawing.Point(359, 412);
            this.monthCalendarDest.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.monthCalendarDest.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.monthCalendarDest.Name = "monthCalendarDest";
            this.monthCalendarDest.TabIndex = 4;
            this.monthCalendarDest.Visible = false;
            this.monthCalendarDest.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarDest_DateChanged);
            this.monthCalendarDest.MouseLeave += new System.EventHandler(this.monthCalendarDest_MouseLeave);
            // 
            // comboBoxCDest
            // 
            this.comboBoxCDest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxCDest.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCDest.FormattingEnabled = true;
            this.comboBoxCDest.Location = new System.Drawing.Point(359, 246);
            this.comboBoxCDest.MaxDropDownItems = 10;
            this.comboBoxCDest.Name = "comboBoxCDest";
            this.comboBoxCDest.Size = new System.Drawing.Size(250, 29);
            this.comboBoxCDest.Sorted = true;
            this.comboBoxCDest.TabIndex = 14;
            this.comboBoxCDest.SelectedIndexChanged += new System.EventHandler(this.comboBoxCDest_SelectedIndexChanged);
            this.comboBoxCDest.TextChanged += new System.EventHandler(this.comboBoxCDest_TextChanged);
            // 
            // comboBoxCOrig
            // 
            this.comboBoxCOrig.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxCOrig.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCOrig.FormattingEnabled = true;
            this.comboBoxCOrig.Location = new System.Drawing.Point(12, 246);
            this.comboBoxCOrig.MaxDropDownItems = 10;
            this.comboBoxCOrig.Name = "comboBoxCOrig";
            this.comboBoxCOrig.Size = new System.Drawing.Size(250, 29);
            this.comboBoxCOrig.Sorted = true;
            this.comboBoxCOrig.TabIndex = 14;
            this.comboBoxCOrig.SelectedIndexChanged += new System.EventHandler(this.comboBoxCOrig_SelectedIndexChanged);
            this.comboBoxCOrig.TextChanged += new System.EventHandler(this.comboBoxCOrig_TextChanged);
            // 
            // dataGridViewAvion
            // 
            this.dataGridViewAvion.BackgroundColor = System.Drawing.Color.Orange;
            this.dataGridViewAvion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewAvion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAvion.Location = new System.Drawing.Point(221, 132);
            this.dataGridViewAvion.Name = "dataGridViewAvion";
            this.dataGridViewAvion.Size = new System.Drawing.Size(450, 73);
            this.dataGridViewAvion.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(356, 225);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 18);
            this.label11.TabIndex = 15;
            this.label11.Text = "Destino";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(9, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 18);
            this.label8.TabIndex = 15;
            this.label8.Text = "Origen";
            // 
            // textBoxPasajero
            // 
            this.textBoxPasajero.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxPasajero.Location = new System.Drawing.Point(221, 79);
            this.textBoxPasajero.Name = "textBoxPasajero";
            this.textBoxPasajero.Size = new System.Drawing.Size(235, 27);
            this.textBoxPasajero.TabIndex = 10;
            // 
            // comboBoxDnis
            // 
            this.comboBoxDnis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxDnis.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDnis.FormattingEnabled = true;
            this.comboBoxDnis.Location = new System.Drawing.Point(94, 53);
            this.comboBoxDnis.MaxDropDownItems = 10;
            this.comboBoxDnis.Name = "comboBoxDnis";
            this.comboBoxDnis.Size = new System.Drawing.Size(121, 29);
            this.comboBoxDnis.Sorted = true;
            this.comboBoxDnis.TabIndex = 9;
            this.comboBoxDnis.SelectedIndexChanged += new System.EventHandler(this.comboBoxDnis_SelectedIndexChanged);
            this.comboBoxDnis.TextChanged += new System.EventHandler(this.comboBoxDnis_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(8, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Avion";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(8, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "DNI";
            // 
            // comboBoxAviones
            // 
            this.comboBoxAviones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxAviones.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.comboBoxAviones.FormattingEnabled = true;
            this.comboBoxAviones.Location = new System.Drawing.Point(94, 132);
            this.comboBoxAviones.Name = "comboBoxAviones";
            this.comboBoxAviones.Size = new System.Drawing.Size(121, 29);
            this.comboBoxAviones.TabIndex = 5;
            this.comboBoxAviones.SelectedIndexChanged += new System.EventHandler(this.comboBoxAviones_SelectedIndexChanged);
            this.comboBoxAviones.TextChanged += new System.EventHandler(this.comboBoxAviones_TextChanged);
            // 
            // clockCU1
            // 
            this.clockCU1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clockCU1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.clockCU1.BackColor = System.Drawing.Color.Transparent;
            this.clockCU1.Location = new System.Drawing.Point(1129, 19);
            this.clockCU1.Name = "clockCU1";
            this.clockCU1.Size = new System.Drawing.Size(64, 0);
            this.clockCU1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(8, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gestion de Pasajes";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(769, -34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(431, 106);
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // tabPasajeros
            // 
            this.tabPasajeros.BackColor = System.Drawing.Color.Orange;
            this.tabPasajeros.Controls.Add(this.textBoxlastname);
            this.tabPasajeros.Controls.Add(this.label14);
            this.tabPasajeros.Controls.Add(this.textBoxnamepila);
            this.tabPasajeros.Controls.Add(this.label13);
            this.tabPasajeros.Controls.Add(this.textBoxdni);
            this.tabPasajeros.Controls.Add(this.Precio);
            this.tabPasajeros.Controls.Add(this.button3);
            this.tabPasajeros.Controls.Add(this.button2);
            this.tabPasajeros.Controls.Add(this.button1);
            this.tabPasajeros.Controls.Add(this.dataGridView2);
            this.tabPasajeros.Controls.Add(this.label2);
            this.tabPasajeros.Controls.Add(this.pictureBox2);
            this.tabPasajeros.Controls.Add(this.clockCU2);
            this.tabPasajeros.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPasajeros.Location = new System.Drawing.Point(4, 34);
            this.tabPasajeros.Name = "tabPasajeros";
            this.tabPasajeros.Padding = new System.Windows.Forms.Padding(3);
            this.tabPasajeros.Size = new System.Drawing.Size(1203, 589);
            this.tabPasajeros.TabIndex = 5;
            this.tabPasajeros.Text = "Gestion de Pasajeros";
            // 
            // textBoxlastname
            // 
            this.textBoxlastname.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxlastname.Location = new System.Drawing.Point(11, 310);
            this.textBoxlastname.Name = "textBoxlastname";
            this.textBoxlastname.Size = new System.Drawing.Size(150, 27);
            this.textBoxlastname.TabIndex = 65;
            this.textBoxlastname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(12, 289);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 18);
            this.label14.TabIndex = 64;
            this.label14.Text = "Apellido";
            // 
            // textBoxnamepila
            // 
            this.textBoxnamepila.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxnamepila.Location = new System.Drawing.Point(11, 233);
            this.textBoxnamepila.Name = "textBoxnamepila";
            this.textBoxnamepila.Size = new System.Drawing.Size(150, 27);
            this.textBoxnamepila.TabIndex = 63;
            this.textBoxnamepila.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(12, 212);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 18);
            this.label13.TabIndex = 62;
            this.label13.Text = "Nombre";
            // 
            // textBoxdni
            // 
            this.textBoxdni.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxdni.Location = new System.Drawing.Point(11, 158);
            this.textBoxdni.Name = "textBoxdni";
            this.textBoxdni.Size = new System.Drawing.Size(150, 27);
            this.textBoxdni.TabIndex = 61;
            this.textBoxdni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Precio
            // 
            this.Precio.AutoSize = true;
            this.Precio.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Precio.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Precio.Location = new System.Drawing.Point(11, 137);
            this.Precio.Name = "Precio";
            this.Precio.Size = new System.Drawing.Size(34, 18);
            this.Precio.TabIndex = 60;
            this.Precio.Text = "DNI";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkOrange;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(343, 78);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(160, 38);
            this.button3.TabIndex = 40;
            this.button3.Text = "Eliminar Pasajero";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkOrange;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(177, 78);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 38);
            this.button2.TabIndex = 39;
            this.button2.Text = "Modificar Pasajero";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkOrange;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(11, 78);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 38);
            this.button1.TabIndex = 38;
            this.button1.Text = "Dar de alta Pasajero";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.BackgroundColor = System.Drawing.Color.DarkOrange;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(769, 78);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(431, 503);
            this.dataGridView2.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(8, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "ABMC de Pasajeros";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(769, -34);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(431, 106);
            this.pictureBox2.TabIndex = 35;
            this.pictureBox2.TabStop = false;
            // 
            // clockCU2
            // 
            this.clockCU2.BackColor = System.Drawing.Color.Transparent;
            this.clockCU2.Location = new System.Drawing.Point(1107, 25);
            this.clockCU2.Name = "clockCU2";
            this.clockCU2.Size = new System.Drawing.Size(84, 26);
            this.clockCU2.TabIndex = 34;
            // 
            // tabAviones
            // 
            this.tabAviones.BackColor = System.Drawing.Color.Orange;
            this.tabAviones.Controls.Add(this.OcupCounter);
            this.tabAviones.Controls.Add(this.label22);
            this.tabAviones.Controls.Add(this.dataGridView6);
            this.tabAviones.Controls.Add(this.textBoxplanecap);
            this.tabAviones.Controls.Add(this.label15);
            this.tabAviones.Controls.Add(this.textBoxplanemodel);
            this.tabAviones.Controls.Add(this.label16);
            this.tabAviones.Controls.Add(this.textBoxplanepatente);
            this.tabAviones.Controls.Add(this.label17);
            this.tabAviones.Controls.Add(this.button4);
            this.tabAviones.Controls.Add(this.button5);
            this.tabAviones.Controls.Add(this.button6);
            this.tabAviones.Controls.Add(this.dataGridView3);
            this.tabAviones.Controls.Add(this.label3);
            this.tabAviones.Controls.Add(this.pictureBox3);
            this.tabAviones.Controls.Add(this.clockCU6);
            this.tabAviones.Controls.Add(this.clockCU3);
            this.tabAviones.Location = new System.Drawing.Point(4, 34);
            this.tabAviones.Name = "tabAviones";
            this.tabAviones.Padding = new System.Windows.Forms.Padding(3);
            this.tabAviones.Size = new System.Drawing.Size(1203, 589);
            this.tabAviones.TabIndex = 6;
            this.tabAviones.Text = "Gestion de Aviones";
            this.tabAviones.Click += new System.EventHandler(this.tabAviones_Click);
            // 
            // OcupCounter
            // 
            this.OcupCounter.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.OcupCounter.Location = new System.Drawing.Point(435, 360);
            this.OcupCounter.Name = "OcupCounter";
            this.OcupCounter.Size = new System.Drawing.Size(58, 27);
            this.OcupCounter.TabIndex = 74;
            this.OcupCounter.Text = "40";
            this.OcupCounter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.OcupCounter.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label22.Location = new System.Drawing.Point(11, 372);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(184, 18);
            this.label22.TabIndex = 73;
            this.label22.Text = "Aviones por Ocupacion";
            // 
            // dataGridView6
            // 
            this.dataGridView6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView6.BackgroundColor = System.Drawing.Color.DarkOrange;
            this.dataGridView6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(15, 393);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(476, 188);
            this.dataGridView6.TabIndex = 72;
            // 
            // textBoxplanecap
            // 
            this.textBoxplanecap.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxplanecap.Location = new System.Drawing.Point(11, 310);
            this.textBoxplanecap.Name = "textBoxplanecap";
            this.textBoxplanecap.Size = new System.Drawing.Size(150, 27);
            this.textBoxplanecap.TabIndex = 71;
            this.textBoxplanecap.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.Location = new System.Drawing.Point(12, 289);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 18);
            this.label15.TabIndex = 70;
            this.label15.Text = "Capacidad";
            // 
            // textBoxplanemodel
            // 
            this.textBoxplanemodel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxplanemodel.Location = new System.Drawing.Point(11, 233);
            this.textBoxplanemodel.Name = "textBoxplanemodel";
            this.textBoxplanemodel.Size = new System.Drawing.Size(150, 27);
            this.textBoxplanemodel.TabIndex = 69;
            this.textBoxplanemodel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(12, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 18);
            this.label16.TabIndex = 68;
            this.label16.Text = "Modelo";
            // 
            // textBoxplanepatente
            // 
            this.textBoxplanepatente.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxplanepatente.Location = new System.Drawing.Point(11, 158);
            this.textBoxplanepatente.Name = "textBoxplanepatente";
            this.textBoxplanepatente.Size = new System.Drawing.Size(150, 27);
            this.textBoxplanepatente.TabIndex = 67;
            this.textBoxplanepatente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(11, 137);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 18);
            this.label17.TabIndex = 66;
            this.label17.Text = "Patente";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkOrange;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(343, 78);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(160, 38);
            this.button4.TabIndex = 43;
            this.button4.Text = "Dar de baja Avion";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkOrange;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button5.Location = new System.Drawing.Point(177, 78);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(160, 38);
            this.button5.TabIndex = 42;
            this.button5.Text = "Modificar Avion";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkOrange;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button6.Location = new System.Drawing.Point(11, 78);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(160, 38);
            this.button6.TabIndex = 41;
            this.button6.Text = "Dar de alta Avion";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView3.BackgroundColor = System.Drawing.Color.DarkOrange;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(769, 78);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(431, 505);
            this.dataGridView3.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(8, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "ABMC de Aviones";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(769, -34);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(431, 106);
            this.pictureBox3.TabIndex = 37;
            this.pictureBox3.TabStop = false;
            // 
            // clockCU6
            // 
            this.clockCU6.BackColor = System.Drawing.Color.Transparent;
            this.clockCU6.Location = new System.Drawing.Point(1107, 25);
            this.clockCU6.Name = "clockCU6";
            this.clockCU6.Size = new System.Drawing.Size(84, 26);
            this.clockCU6.TabIndex = 36;
            // 
            // clockCU3
            // 
            this.clockCU3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clockCU3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.clockCU3.BackColor = System.Drawing.Color.Transparent;
            this.clockCU3.Location = new System.Drawing.Point(966, 6);
            this.clockCU3.Name = "clockCU3";
            this.clockCU3.Size = new System.Drawing.Size(58, 0);
            this.clockCU3.TabIndex = 2;
            // 
            // tabDestinos
            // 
            this.tabDestinos.BackColor = System.Drawing.Color.Orange;
            this.tabDestinos.Controls.Add(this.viewcityButton);
            this.tabDestinos.Controls.Add(this.textBoxLong);
            this.tabDestinos.Controls.Add(this.label21);
            this.tabDestinos.Controls.Add(this.textBoxLat);
            this.tabDestinos.Controls.Add(this.label20);
            this.tabDestinos.Controls.Add(this.textBoxcityname);
            this.tabDestinos.Controls.Add(this.label19);
            this.tabDestinos.Controls.Add(this.button12);
            this.tabDestinos.Controls.Add(this.textBoxcountryname);
            this.tabDestinos.Controls.Add(this.label18);
            this.tabDestinos.Controls.Add(this.dataGridView5);
            this.tabDestinos.Controls.Add(this.button10);
            this.tabDestinos.Controls.Add(this.button11);
            this.tabDestinos.Controls.Add(this.button7);
            this.tabDestinos.Controls.Add(this.button8);
            this.tabDestinos.Controls.Add(this.button9);
            this.tabDestinos.Controls.Add(this.dataGridView4);
            this.tabDestinos.Controls.Add(this.label4);
            this.tabDestinos.Controls.Add(this.pictureBox4);
            this.tabDestinos.Controls.Add(this.clockCU7);
            this.tabDestinos.Controls.Add(this.clockCU4);
            this.tabDestinos.Location = new System.Drawing.Point(4, 34);
            this.tabDestinos.Name = "tabDestinos";
            this.tabDestinos.Padding = new System.Windows.Forms.Padding(3);
            this.tabDestinos.Size = new System.Drawing.Size(1203, 589);
            this.tabDestinos.TabIndex = 7;
            this.tabDestinos.Text = "Gestion de Destinos";
            // 
            // viewcityButton
            // 
            this.viewcityButton.BackColor = System.Drawing.Color.DarkOrange;
            this.viewcityButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.viewcityButton.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.viewcityButton.Location = new System.Drawing.Point(343, 485);
            this.viewcityButton.Name = "viewcityButton";
            this.viewcityButton.Size = new System.Drawing.Size(160, 43);
            this.viewcityButton.TabIndex = 77;
            this.viewcityButton.Text = "Ver en el Mapa";
            this.viewcityButton.UseVisualStyleBackColor = false;
            this.viewcityButton.Click += new System.EventHandler(this.viewcityButton_Click);
            // 
            // textBoxLong
            // 
            this.textBoxLong.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxLong.Location = new System.Drawing.Point(92, 521);
            this.textBoxLong.Name = "textBoxLong";
            this.textBoxLong.Size = new System.Drawing.Size(150, 27);
            this.textBoxLong.TabIndex = 76;
            this.textBoxLong.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxLong.TextChanged += new System.EventHandler(this.textBoxLong_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(93, 500);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 18);
            this.label21.TabIndex = 75;
            this.label21.Text = "Longitud";
            // 
            // textBoxLat
            // 
            this.textBoxLat.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxLat.Location = new System.Drawing.Point(92, 461);
            this.textBoxLat.Name = "textBoxLat";
            this.textBoxLat.Size = new System.Drawing.Size(150, 27);
            this.textBoxLat.TabIndex = 74;
            this.textBoxLat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxLat.TextChanged += new System.EventHandler(this.textBoxLat_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(93, 440);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 18);
            this.label20.TabIndex = 73;
            this.label20.Text = "Latitud";
            // 
            // textBoxcityname
            // 
            this.textBoxcityname.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxcityname.Location = new System.Drawing.Point(11, 399);
            this.textBoxcityname.Name = "textBoxcityname";
            this.textBoxcityname.Size = new System.Drawing.Size(150, 27);
            this.textBoxcityname.TabIndex = 72;
            this.textBoxcityname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(12, 378);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 18);
            this.label19.TabIndex = 71;
            this.label19.Text = "Nombre";
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.DarkOrange;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button12.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button12.Location = new System.Drawing.Point(11, 302);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(160, 38);
            this.button12.TabIndex = 70;
            this.button12.Text = "Alta de Ciudad";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // textBoxcountryname
            // 
            this.textBoxcountryname.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxcountryname.Location = new System.Drawing.Point(11, 175);
            this.textBoxcountryname.Name = "textBoxcountryname";
            this.textBoxcountryname.Size = new System.Drawing.Size(150, 27);
            this.textBoxcountryname.TabIndex = 69;
            this.textBoxcountryname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(12, 154);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 18);
            this.label18.TabIndex = 68;
            this.label18.Text = "Nombre";
            // 
            // dataGridView5
            // 
            this.dataGridView5.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dataGridView5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView5.BackgroundColor = System.Drawing.Color.DarkOrange;
            this.dataGridView5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(650, 78);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(541, 174);
            this.dataGridView5.TabIndex = 47;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.DarkOrange;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button10.Location = new System.Drawing.Point(343, 302);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(160, 38);
            this.button10.TabIndex = 46;
            this.button10.Text = "Baja de Ciudad";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.DarkOrange;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button11.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button11.Location = new System.Drawing.Point(177, 302);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(160, 38);
            this.button11.TabIndex = 45;
            this.button11.Text = "Modificar Ciudad";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DarkOrange;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button7.Location = new System.Drawing.Point(343, 78);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(160, 38);
            this.button7.TabIndex = 43;
            this.button7.Text = "Baja de Pais";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DarkOrange;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button8.Location = new System.Drawing.Point(177, 78);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(160, 38);
            this.button8.TabIndex = 42;
            this.button8.Text = "Modificar Pais";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkOrange;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button9.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button9.Location = new System.Drawing.Point(11, 78);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(160, 38);
            this.button9.TabIndex = 41;
            this.button9.Text = "Alta de Pais";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dataGridView4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView4.BackgroundColor = System.Drawing.Color.DarkOrange;
            this.dataGridView4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(650, 302);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(541, 262);
            this.dataGridView4.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(8, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Consulta de Destinos";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(769, -34);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(431, 106);
            this.pictureBox4.TabIndex = 37;
            this.pictureBox4.TabStop = false;
            // 
            // clockCU7
            // 
            this.clockCU7.BackColor = System.Drawing.Color.Transparent;
            this.clockCU7.Location = new System.Drawing.Point(1107, 25);
            this.clockCU7.Name = "clockCU7";
            this.clockCU7.Size = new System.Drawing.Size(84, 26);
            this.clockCU7.TabIndex = 36;
            // 
            // clockCU4
            // 
            this.clockCU4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clockCU4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.clockCU4.BackColor = System.Drawing.Color.Transparent;
            this.clockCU4.Location = new System.Drawing.Point(966, 6);
            this.clockCU4.Name = "clockCU4";
            this.clockCU4.Size = new System.Drawing.Size(58, 0);
            this.clockCU4.TabIndex = 2;
            // 
            // tabAnalitycs
            // 
            this.tabAnalitycs.BackColor = System.Drawing.Color.SkyBlue;
            this.tabAnalitycs.Controls.Add(this.buttonDepu);
            this.tabAnalitycs.Controls.Add(this.dateTimePicker1);
            this.tabAnalitycs.Controls.Add(this.textBoxdestino);
            this.tabAnalitycs.Controls.Add(this.comboBoxdestinos);
            this.tabAnalitycs.Controls.Add(this.radioButtonnternacinoal);
            this.tabAnalitycs.Controls.Add(this.radioButtonNacinoal);
            this.tabAnalitycs.Controls.Add(this.CommitQuery);
            this.tabAnalitycs.Controls.Add(this.label23);
            this.tabAnalitycs.Controls.Add(this.label24);
            this.tabAnalitycs.Controls.Add(this.label25);
            this.tabAnalitycs.Controls.Add(this.dataGridView1);
            this.tabAnalitycs.Controls.Add(this.label5);
            this.tabAnalitycs.Controls.Add(this.pictureBox5);
            this.tabAnalitycs.Controls.Add(this.clockCU8);
            this.tabAnalitycs.Location = new System.Drawing.Point(4, 34);
            this.tabAnalitycs.Name = "tabAnalitycs";
            this.tabAnalitycs.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnalitycs.Size = new System.Drawing.Size(1203, 589);
            this.tabAnalitycs.TabIndex = 8;
            this.tabAnalitycs.Text = "Analiticas";
            // 
            // buttonDepu
            // 
            this.buttonDepu.BackColor = System.Drawing.Color.LightSkyBlue;
            this.buttonDepu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDepu.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.buttonDepu.Location = new System.Drawing.Point(11, 538);
            this.buttonDepu.Name = "buttonDepu";
            this.buttonDepu.Size = new System.Drawing.Size(160, 43);
            this.buttonDepu.TabIndex = 90;
            this.buttonDepu.Text = "Depuracion";
            this.buttonDepu.UseVisualStyleBackColor = false;
            this.buttonDepu.Click += new System.EventHandler(this.buttonDepu_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(11, 249);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 89;
            // 
            // textBoxdestino
            // 
            this.textBoxdestino.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxdestino.Location = new System.Drawing.Point(11, 161);
            this.textBoxdestino.Name = "textBoxdestino";
            this.textBoxdestino.Size = new System.Drawing.Size(250, 27);
            this.textBoxdestino.TabIndex = 88;
            // 
            // comboBoxdestinos
            // 
            this.comboBoxdestinos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxdestinos.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxdestinos.FormattingEnabled = true;
            this.comboBoxdestinos.Location = new System.Drawing.Point(11, 113);
            this.comboBoxdestinos.MaxDropDownItems = 10;
            this.comboBoxdestinos.Name = "comboBoxdestinos";
            this.comboBoxdestinos.Size = new System.Drawing.Size(250, 29);
            this.comboBoxdestinos.Sorted = true;
            this.comboBoxdestinos.TabIndex = 87;
            this.comboBoxdestinos.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBoxdestinos.TextChanged += new System.EventHandler(this.comboBoxdestinos_TextChanged);
            // 
            // radioButtonnternacinoal
            // 
            this.radioButtonnternacinoal.AutoSize = true;
            this.radioButtonnternacinoal.Location = new System.Drawing.Point(15, 333);
            this.radioButtonnternacinoal.Name = "radioButtonnternacinoal";
            this.radioButtonnternacinoal.Size = new System.Drawing.Size(86, 17);
            this.radioButtonnternacinoal.TabIndex = 86;
            this.radioButtonnternacinoal.Text = "Internacional";
            this.radioButtonnternacinoal.UseVisualStyleBackColor = true;
            // 
            // radioButtonNacinoal
            // 
            this.radioButtonNacinoal.AutoSize = true;
            this.radioButtonNacinoal.Checked = true;
            this.radioButtonNacinoal.Location = new System.Drawing.Point(15, 310);
            this.radioButtonNacinoal.Name = "radioButtonNacinoal";
            this.radioButtonNacinoal.Size = new System.Drawing.Size(67, 17);
            this.radioButtonNacinoal.TabIndex = 85;
            this.radioButtonNacinoal.TabStop = true;
            this.radioButtonNacinoal.Text = "Nacional";
            this.radioButtonNacinoal.UseVisualStyleBackColor = true;
            // 
            // CommitQuery
            // 
            this.CommitQuery.BackColor = System.Drawing.Color.LightSkyBlue;
            this.CommitQuery.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CommitQuery.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.CommitQuery.Location = new System.Drawing.Point(11, 371);
            this.CommitQuery.Name = "CommitQuery";
            this.CommitQuery.Size = new System.Drawing.Size(160, 43);
            this.CommitQuery.TabIndex = 84;
            this.CommitQuery.Text = "Consultar";
            this.CommitQuery.UseVisualStyleBackColor = false;
            this.CommitQuery.Click += new System.EventHandler(this.CommitQuery_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(12, 289);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 18);
            this.label23.TabIndex = 82;
            this.label23.Text = "Tipo";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(8, 207);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(128, 18);
            this.label24.TabIndex = 80;
            this.label24.Text = "Fecha de Salida";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label25.Location = new System.Drawing.Point(8, 78);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(62, 18);
            this.label25.TabIndex = 78;
            this.label25.Text = "Destino";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.LightSkyBlue;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(343, 113);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(854, 468);
            this.dataGridView1.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(8, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Analiticas de Negocio";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(766, -34);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(431, 106);
            this.pictureBox5.TabIndex = 37;
            this.pictureBox5.TabStop = false;
            // 
            // clockCU8
            // 
            this.clockCU8.BackColor = System.Drawing.Color.Transparent;
            this.clockCU8.Location = new System.Drawing.Point(1104, 25);
            this.clockCU8.Name = "clockCU8";
            this.clockCU8.Size = new System.Drawing.Size(84, 26);
            this.clockCU8.TabIndex = 36;
            // 
            // clock1
            // 
            this.clock1.BackColor = System.Drawing.Color.Transparent;
            this.clock1.Location = new System.Drawing.Point(0, 0);
            this.clock1.Name = "clock1";
            this.clock1.Size = new System.Drawing.Size(100, 26);
            this.clock1.TabIndex = 0;
            // 
            // MainAPP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1211, 627);
            this.Controls.Add(this.tabControl);
            this.Name = "MainAPP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestor de Pasajes";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPasajes.ResumeLayout(false);
            this.tabPasajes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPasajes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPasajero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAvion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPasajeros.ResumeLayout(false);
            this.tabPasajeros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabAviones.ResumeLayout(false);
            this.tabAviones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabDestinos.ResumeLayout(false);
            this.tabDestinos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.tabAnalitycs.ResumeLayout(false);
            this.tabAnalitycs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPasajes;
        private System.Windows.Forms.TabPage tabPasajeros;
        private System.Windows.Forms.TabPage tabAviones;
        private System.Windows.Forms.TabPage tabDestinos;
        private System.Windows.Forms.TabPage tabAnalitycs;
        private System.Windows.Forms.Label label1;
        private ClockCU clock1;
        private ClockCU clockCU1;
        private ClockCU clockCU3;
        private ClockCU clockCU4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxAviones;
        private System.Windows.Forms.MonthCalendar monthCalendarOrig;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxDnis;
        private System.Windows.Forms.TextBox textBoxPasajero;
        private System.Windows.Forms.DataGridView dataGridViewAvion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxCOrig;
        private System.Windows.Forms.MonthCalendar monthCalendarDest;
        private System.Windows.Forms.ComboBox comboBoxCDest;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox FechaDestTxt;
        private System.Windows.Forms.TextBox FechaOrigenTxt;
        public System.Windows.Forms.RadioButton radioButtonSI;
        private System.Windows.Forms.RadioButton radioButtonIyV;
        private System.Windows.Forms.TextBox textBoxCDest;
        private System.Windows.Forms.TextBox textBoxCOrig;
        private ClockCU clockCU5;
        private System.Windows.Forms.DataGridView dataGridViewPasajero;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelErrorEleccionDiudades;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonconfirmarPasaje;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonshowCiudadDestino;
        private System.Windows.Forms.Button buttonshowCiudadOrigen;
        private System.Windows.Forms.DataGridView dataGridViewPasajes;
        private System.Windows.Forms.Button cancelPasajeButton;
        private ClockCU clockCU2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private ClockCU clockCU6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private ClockCU clockCU7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private ClockCU clockCU8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.TextBox textBoxlastname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxnamepila;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxdni;
        private System.Windows.Forms.Label Precio;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.TextBox textBoxplanecap;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxplanemodel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxplanepatente;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxcityname;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox textBoxcountryname;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button viewcityButton;
        private System.Windows.Forms.TextBox textBoxLong;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxLat;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.TextBox OcupCounter;
        private System.Windows.Forms.Button CommitQuery;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RadioButton radioButtonnternacinoal;
        private System.Windows.Forms.RadioButton radioButtonNacinoal;
        private System.Windows.Forms.TextBox textBoxdestino;
        private System.Windows.Forms.ComboBox comboBoxdestinos;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button buttonDepu;
    }
}

