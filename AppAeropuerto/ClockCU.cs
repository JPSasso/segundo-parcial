﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppAeropuerto
{
    public partial class ClockCU : UserControl
    {
        public ClockCU()
        {
            InitializeComponent();
        }
        private void Clock_Load(object sender, EventArgs e)
        {
            //timer1.Start();
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new MethodInvoker(delegate () {
                label1.Text = DateTime.Now.ToString("hh:mm tt");  
            }));
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString("hh:mm tt");
        }
    }
}
