﻿using BLL;
using BE;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace AppAeropuerto
{
    public partial class MainAPP : Form
    {
        #region Consignas
        /*
         Un aeropuerto necesita una aplicación que permita registrar los arribos y despegues de los diferentes vuelos. 
         Los vuelos pueden ser nacionales e internacionales, en éste último se registra además de la ciudad de destino 
         el país, y los lugares donde puede llegar a hacer escala. En todos los casos se debe registrar 
         la fecha/hora de partida y la fecha/hora tentativa de llegada.

        Se requiere:

       X Alta y Cancelación de vuelos
       X ABM Destinos
       X  ABM de pasajeros
        X Listar vuelos por las combinaciones de Destino, Fecha y Tipo, 
            cada listado debe mostrar todos los datos del vuelo, los el detalle de los pasajeros, 
            y el porcentaje de ocupación.
       X Listar los vuelos finalizados cuya ocupación sea menor al 40%.

        Requerimientos:

       X Windows Forms
       X C#
       X Arquitectura 4 capas
       X Base de Datos Sql Server
       X 1 control de usuario mínimo
       X Abrir mediante un proceso un navegador con el parámetro de google maps para visualizar los destinos: 
            Ej: https://www.google.com.ar/maps/@-34.6342485,-58.469787,14z
             (Las coordenadas deben ser en formato WGS84)
        
        */
        #endregion
        #region variables
        readonly PasajeroBLL pasajeroBLL = new PasajeroBLL();
        readonly AvionBLL avionBLL = new AvionBLL();
        readonly CiudadBLL ciudadBLL = new CiudadBLL();
        readonly PaisBLL paisBLL = new PaisBLL();
        readonly PasajeBLL pasajeBLL = new PasajeBLL();
        int CantPasajes = 2;
        readonly googleMapsAssistant GMA = new googleMapsAssistant();
        #endregion
        public MainAPP()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OcupCounter.Text = "40";
            OrquestarOrigenesDeDatos();
            pasajeBLL.fechaLimite = 5;
            dataGridViewPasajes.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView5.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridViewPasajero.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridViewAvion.SelectionMode = DataGridViewSelectionMode.FullRowSelect;            
        }

        private void OrquestarOrigenesDeDatos()
        {
            FillComboBoxDNIs();
            FillComboBoxAviones();
            FillComboBoxCiudades();
            LlenarGrillaPasajes();
            LlenarDataGridPasajeros();
            LlenarGrillaAviones();
            LlenarGrillaCiudades();
            LlenarGrillaPaises();
        }

        #region Altas
        private void button1_Click(object sender, EventArgs e)
        //pasajeros
        {
            Pasajero P = new Pasajero
            {
                DNI = int.Parse(textBoxdni.Text),
                Nombre = textBoxnamepila.Text,
                Apellido = textBoxlastname.Text
            };
            int rto = pasajeroBLL.AltaPasajero(P);
            if (rto > 0)
            {
                LlenarDataGridPasajeros();
                FillComboBoxDNIs();
            }
        }
        private void button6_Click(object sender, EventArgs e)
        //aviones
        {
            Avion A = new Avion()
            {
                Patente = int.Parse(textBoxplanepatente.Text),
                Capacidad = int.Parse(textBoxplanecap.Text),
                Modelo = textBoxplanemodel.Text,
                Ocupacion = 0,
                Pasajeros = new List<Pasajero>()
            };
            int rto = avionBLL.AltaAvion(A);
            if (rto > 0)
            {
                LlenarGrillaAviones();
                FillComboBoxAviones();
            }
        }
        private void button9_Click(object sender, EventArgs e)
        //paises
        {
            Pais P = new Pais()
            {
                Nombre = textBoxcountryname.Text
            };
            int rto = paisBLL.AltaPais(P);
            if (rto > 0)
            {
                LlenarGrillaPaises();
            }
        }
        private void button12_Click(object sender, EventArgs e)
        //ciudades
        {
            Ciudad C = new Ciudad()
            {
                Nombre = textBoxcityname.Text,
                Latitud = float.Parse( textBoxLat.Text),
                Longitud = float.Parse(textBoxLong.Text),
                Pais = (Pais)dataGridView5.SelectedRows[0].DataBoundItem
            };
            int rto = ciudadBLL.AltaCiudad(C);
            if(rto >0)
            {
                LlenarGrillaCiudades();
                FillComboBoxCiudades();
            }
        }
        #endregion
        #region Modificaiones
        private void button2_Click(object sender, EventArgs e)
        //pasajeros
        {
            Pasajero P = (Pasajero)dataGridView2.SelectedRows[0].DataBoundItem;
            P.Nombre = Interaction.InputBox("Nombre", "", P.Nombre);
            P.Apellido = Interaction.InputBox("Apellido", "", P.Apellido);
            int rto = pasajeroBLL.ModificarPasajero(P);
            if (rto >= 0)
            {
                LlenarDataGridPasajeros();
                FillComboBoxDNIs();
            }
        }
        private void button5_Click(object sender, EventArgs e)
        //aviones
        {
            Avion A = (Avion)dataGridView3.SelectedRows[0].DataBoundItem;
            A.Modelo = Interaction.InputBox("Modelo", "", A.Modelo);
            A.Capacidad = int.Parse(Interaction.InputBox("Capacidad", "", A.Capacidad.ToString()));
            int rto = avionBLL.ModificarAvion(A);
            if(rto >= 0)
            {
                LlenarGrillaAviones();
                FillComboBoxAviones();
            }
        }
        private void button8_Click(object sender, EventArgs e)
        //paises
        {
            Pais P = (Pais)dataGridView5.SelectedRows[0].DataBoundItem;
            string nombre = Interaction.InputBox("Nombre", "", P.Nombre);
            int rto = paisBLL.ModificarPais(P, nombre);
            if(rto > 0)
            {
                P.Nombre = nombre;
                LlenarGrillaPaises();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        //ciudades
        {
            Ciudad C = (Ciudad)dataGridView4.SelectedRows[0].DataBoundItem;
            C.Nombre = Interaction.InputBox("Nombre", "", C.Nombre);
            C.Latitud = float.Parse(Interaction.InputBox("Ingrese latitud: ", "", C.Latitud.ToString()));
            C.Longitud = float.Parse(Interaction.InputBox("Ingrese longitud: ","",C.Longitud.ToString()));
            if (dataGridView5.SelectedRows[0] != null){
                C.Pais = (Pais)dataGridView5.SelectedRows[0].DataBoundItem;
            }
            int rto = ciudadBLL.ModificarCiudad(C);
            if (rto >= 0)
            {
                LlenarGrillaCiudades();
                FillComboBoxCiudades();
            }
        }
        #endregion
        #region Bajas
        private void button3_Click(object sender, EventArgs e)
        //pasajeros
        {
            Pasajero P = (Pasajero)dataGridView2.SelectedRows[0].DataBoundItem;
            int rto = pasajeroBLL.BajaPasajero(P);
            if (rto > 0)
            {
                LlenarDataGridPasajeros();
                FillComboBoxDNIs();
            }
        }
        private void button4_Click(object sender, EventArgs e)
        //aviones
        {
            Avion A = (Avion)dataGridView3.SelectedRows[0].DataBoundItem;
            int rto = avionBLL.BajaAvion(A);
            if (rto >= 0)
            {
                LlenarGrillaAviones();
                FillComboBoxAviones();
            }
        }
        private void button7_Click(object sender, EventArgs e)
        //paises
        {
            Pais P = (Pais)dataGridView5.SelectedRows[0].DataBoundItem;
            int rto = paisBLL.BajaPais(P);
            if (rto > 0)
            {
                LlenarGrillaPaises();
            }
        }
        private void button10_Click(object sender, EventArgs e)
        //xiudades
        {
            Ciudad C = (Ciudad)dataGridView4.SelectedRows[0].DataBoundItem;
            int rto = ciudadBLL.BajaCiudad(C);
            if(rto >=0)
            {
                LlenarGrillaCiudades();
                FillComboBoxCiudades();
            }
        }
        #endregion
        #region Fillers
        private void FillComboBoxDNIs()
        {
            comboBoxDnis.Items.Clear();
            foreach (Pasajero P in pasajeroBLL.ListarPasajeros())
            {
                comboBoxDnis.Items.Add(P.DNI);
            }
        }
        private void FillComboBoxAviones()
        {
            comboBoxAviones.Items.Clear();
            foreach (Avion a in avionBLL.ListarAviones())
            {
                comboBoxAviones.Items.Add(a.Modelo);
            }
        }
        private void FillComboBoxCiudades()
        {
            comboBoxCOrig.Items.Clear();
            comboBoxCDest.Items.Clear();
            comboBoxdestinos.Items.Clear();
            foreach (Ciudad c in ciudadBLL.ListarCiudades())
            {
                comboBoxCOrig.Items.Add(c.Nombre);
                comboBoxCDest.Items.Add(c.Nombre);
                comboBoxdestinos.Items.Add(c.Nombre);
            }
        }
        private void LlenarGrillaPaises()
        {
            dataGridView5.DataSource = null;
            dataGridView5.DataSource = paisBLL.ListarPaises();
        }
        private void LlenarGrillaCiudades()
        {
            dataGridView4.DataSource = null;
            dataGridView4.DataSource = ciudadBLL.ListarCiudades();
        }
        private void LlenarGrillaAviones()
        {
            List<Avion> aviones = avionBLL.ListarAviones();
            List<Avion> avionesPerdedores = new List<Avion>(); 
            dataGridView3.DataSource = null;
            dataGridView3.DataSource = aviones;
            foreach (Avion A in aviones)
            {
                try
                {
                    if (A.Ocupacion <= int.Parse(OcupCounter.Text))
                    {
                        avionesPerdedores.Add(A);
                    }
                }
                catch(Exception)
                {
                    OcupCounter.Text = "0";                    
                }
            }
            dataGridView6.DataSource = null;
            dataGridView6.DataSource = avionesPerdedores;
        }
        private void LlenarGrillaPasajes()
        {
            dataGridViewPasajes.DataSource = null;
            dataGridViewPasajes.DataSource = pasajeBLL.ObtenerPasajes();
        }
        private void LlenarDataGridPasajeros()
        {
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = pasajeroBLL.ListarPasajeros();
        }
        #endregion

        private void tabPasajes_Click(object sender, EventArgs e)
        {
            /// Este control debe permitir: X Alta y Baja de pasajes
            ///                                 * Tener en cuenta que no se pueden modificar pasajes ya en vuelo
            ///                             Seleccionar Destino y Origen de una lista desplegable -> mostrar link a GoogleMaps
            ///                            X Buscar Pasajero por DNI -> mostrar toda la info
            ///                            X Buscar Avion por patente -> mostrar toda la info
            ///            
        }

        private void tabAviones_Click(object sender, EventArgs e)
        {
            /// Este control debe permitir: ABM de Aviones
            ///                             Consulta de Aviones
            ///                             Resaltar los aviones con menos de 40% de ocupacion
            ///   

        }

        private void comboBoxDnis_TextChanged(object sender, EventArgs e)
        {
            comboBoxDnis.Items.Clear();
            try
            {
                foreach (Pasajero P in pasajeroBLL.GetPasajerosWDNI(int.Parse(comboBoxDnis.Text)))
                {
                    comboBoxDnis.Items.Add(P.DNI);
                }
                textBoxPasajero.Text = comboBoxDnis.Text;
                comboBoxDnis.Focus();
                comboBoxDnis.SelectionStart = comboBoxDnis.Text.Length;
            }
            catch(Exception) { FillComboBoxDNIs(); }
        }
        
        private void comboBoxAviones_TextChanged(object sender, EventArgs e)
        {
            comboBoxAviones.Items.Clear();
            try
            {
                foreach (Avion a in avionBLL.ListarAviones())
                {
                    comboBoxAviones.Items.Add(a.Modelo);
                }
            }
            catch (Exception) { FillComboBoxAviones(); }
        }

        private void comboBoxAviones_SelectedIndexChanged(object sender, EventArgs e)
        {
            Avion a = avionBLL.GetAvionWModel(comboBoxAviones.Text);
            dataGridViewAvion.DataSource = null;
            List<Avion> avion = new List<Avion>
            {
                a
            };
            dataGridViewAvion.DataSource =avion;            
        }
        
        private void textBox3_Click(object sender, EventArgs e)
        {
            monthCalendarOrig.Visible = true;
        }

        private void monthCalendarOrig_DateChanged(object sender, DateRangeEventArgs e)
        {
            FechaOrigenTxt.Text = monthCalendarOrig.SelectionStart.Date.ToString("dd/MM/yyyy");
            monthCalendarOrig.Visible = false;
        }

        private void radioButtonIyV_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonSI.Checked == true)
            {
                FechaDestTxt.Visible = false;
                FechaDestTxt.Text = "";
                CantPasajes = 1;
                label12.Visible = false;
                monthCalendarDest.Visible = false;
            }
            else
            {
                FechaDestTxt.Visible = true;
                label12.Visible = true;
                CantPasajes = 2;
            }
        }

        private void FechaDestTxt_Click(object sender, EventArgs e)
        {
            monthCalendarDest.Visible = true;
        }

        private void monthCalendarDest_DateChanged(object sender, DateRangeEventArgs e)
        {
            FechaDestTxt.Text = monthCalendarDest.SelectionStart.Date.ToString("dd/MM/yyyy");
            monthCalendarDest.Visible = false;
        }

        private void comboBoxDnis_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pasajero p = pasajeroBLL.GetPasajero(int.Parse(textBoxPasajero.Text));
            dataGridViewPasajero.DataSource = null;
            List<Pasajero> pasajeros = new List<Pasajero>
            {
                p
            };
            dataGridViewPasajero.DataSource = pasajeros;
        }

         private void comboBoxCOrig_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillComboBoxCiudades();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            FechaOrigenTxt.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }

        private void comboBoxCOrig_TextChanged(object sender, EventArgs e)
        {
            comboBoxCOrig.Items.Clear();
            try
            {
                Ciudad showC = new Ciudad();
                foreach (Ciudad C in ciudadBLL.GetCiudadesByName(comboBoxCOrig.Text))
                {
                    comboBoxCOrig.Items.Add(C.Nombre);                    
                    showC.Pais = C.Pais;
                }
                comboBoxCOrig.Focus();
                comboBoxCOrig.SelectionStart = comboBoxCOrig.Text.Length;

                if (comboBoxCOrig.Items.Count == 1)
                {
                    textBoxCOrig.Text = comboBoxCOrig.Text + "," + showC.Pais.Nombre;
                }
                else textBoxCOrig.Text = comboBoxCOrig.Text;
            }
            catch (Exception) { FillComboBoxCiudades(); }
        }

        private void comboBoxCDest_TextChanged(object sender, EventArgs e)
        {
            comboBoxCDest.Items.Clear();
            try
            {
                Ciudad showC = new Ciudad();
                foreach (Ciudad C in ciudadBLL.GetCiudadesByName(comboBoxCDest.Text))
                {
                    comboBoxCDest.Items.Add(C.Nombre);
                    showC.Pais = C.Pais;
                }
                comboBoxCDest.Focus();
                comboBoxCDest.SelectionStart = comboBoxCDest.Text.Length;

                if (comboBoxCDest.Items.Count == 1)
                {
                    textBoxCDest.Text = comboBoxCDest.Text + "," + showC.Pais.Nombre;
                }
                else textBoxCDest.Text = comboBoxCDest.Text;

                if (textBoxCDest.Text == textBoxCOrig.Text)
                {
                    textBoxCDest.Text = "";
                    labelErrorEleccionDiudades.Visible = true;
                    label12.Visible = false;
                }
                else
                {
                    labelErrorEleccionDiudades.Visible = false;
                    label12.Visible = true;
                }
            }
            catch (Exception) { FillComboBoxCiudades(); }
        }

        private void comboBoxCDest_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillComboBoxCiudades();
        }

        private void FechaOrigenTxt_TextChanged(object sender, EventArgs e)
        {
            monthCalendarOrig.Visible = false;
        }
        private void monthCalendarOrig_MouseLeave(object sender, EventArgs e)
        {
            monthCalendarOrig.Visible = false;
        }
        private void monthCalendarDest_MouseLeave(object sender, EventArgs e)
        {
            monthCalendarDest.Visible = false;
        }
        #region pasaje
        private void buttonconfirmarPasaje_Click(object sender, EventArgs e)
        {
            Pasaje pasajeIda = new Pasaje();
            Pasaje pasajeVuelta = new Pasaje();
            if (DateTime.Parse(FechaOrigenTxt.Text) > DateTime.Today.AddDays(pasajeBLL.fechaLimite)) 
            {
                if (CantPasajes == 1)
                {
                    pasajeIda = new Pasaje
                    {
                        Avion = (Avion)dataGridViewAvion.Rows[0].DataBoundItem,
                        Pasajero = (Pasajero)dataGridViewPasajero.Rows[0].DataBoundItem,
                        _cOrigen = ciudadBLL.GetCiudadByName(textBoxCOrig.Text.Split(',')[0]),
                        _cDestino = ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0]),
                        FechaOrigen = DateTime.Parse(FechaOrigenTxt.Text),
                        FechaLlegada = DateTime.Parse("1/1/1000"),
                        Tipo = pasajeBLL.GetTipoDeViaje(ciudadBLL.GetCiudadByName(textBoxCOrig.Text.Split(',')[0]),
                        ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0])),
                        NumeroPasaje = pasajeBLL.CrearNumeroDePasaje(),
                        Escalas = new List<Escala>(),
                        Precio = 0f
                    };
                    Pasaje_loader pasaje_Loader = new Pasaje_loader
                    {
                        pasajeIda = pasajeIda,
                        pasajevuelta = null
                    };
                    pasaje_Loader.ShowDialog();
                    pasajeIda = pasaje_Loader.pasajeIda;
                    pasajeVuelta = null;
                    pasaje_Loader.Dispose();
                }
                if (CantPasajes == 2)
                {
                    if (FechaDestTxt.Text != null && DateTime.Parse(FechaDestTxt.Text) > DateTime.Parse(FechaOrigenTxt.Text))
                    {
                        pasajeIda = new Pasaje
                        {
                            Avion = (Avion)dataGridViewAvion.Rows[0].DataBoundItem,
                            Pasajero = (Pasajero)dataGridViewPasajero.Rows[0].DataBoundItem,
                            _cOrigen = ciudadBLL.GetCiudadByName(textBoxCOrig.Text.Split(',')[0]),
                            _cDestino = ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0]),
                            FechaOrigen = DateTime.Parse(FechaOrigenTxt.Text),
                            FechaLlegada = DateTime.Parse("1/1/1000"),
                            Tipo = pasajeBLL.GetTipoDeViaje(ciudadBLL.GetCiudadByName(textBoxCOrig.Text.Split(',')[0]),
                          ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0])),
                            NumeroPasaje = pasajeBLL.CrearNumeroDePasaje(),
                            Escalas = new List<Escala>(),
                            Precio = 0f
                        }; 
                        pasajeVuelta = new Pasaje
                        {
                            Avion = (Avion)dataGridViewAvion.Rows[0].DataBoundItem,
                            Pasajero = (Pasajero)dataGridViewPasajero.Rows[0].DataBoundItem,
                            _cOrigen = ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0]),
                            _cDestino = ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0]),
                            FechaOrigen = DateTime.Parse(FechaDestTxt.Text),
                            FechaLlegada = DateTime.Parse(FechaOrigenTxt.Text),
                            Tipo = pasajeIda.Tipo,
                            NumeroPasaje = pasajeIda.NumeroPasaje + 1,
                            Escalas = pasajeIda.Escalas,
                            Precio = pasajeIda.Precio
                        };
                        Pasaje_loader pasaje_Loader = new Pasaje_loader
                        {
                            pasajeIda = pasajeIda,
                            pasajevuelta = pasajeVuelta
                        };
                        pasaje_Loader.ShowDialog();
                        pasajeIda = pasaje_Loader.pasajeIda;
                        pasajeVuelta = pasaje_Loader.pasajevuelta;
                        pasaje_Loader.Dispose();
                    }
                }
                int rtoPasaje = pasajeBLL.CrearPasaje(pasajeIda); 
                int rtoPasajeV = pasajeBLL.CrearPasaje(pasajeVuelta);
                if ((rtoPasaje > 0) || (rtoPasaje > 0 && rtoPasajeV > 0))
                {
                    MessageBox.Show("Pasaje creado Exitosamente");
                    LlenarGrillaPasajes();
                }
                else if (rtoPasaje == -3)
                {
                    if (pasajeIda != null || pasajeIda.Avion != null)
                    {
                        MessageBox.Show("El avion " + pasajeIda.Avion.Patente.ToString()
                            + " no tiene asientos disponibles");
                    }
                }
            }
            else
            {
                MessageBox.Show("No se puede realizar este viaje\n\nDias minimos para solicitar un pasaje: "
                    + pasajeBLL.fechaLimite.ToString());
            }             
        }
        private void cancelPasajeButton_Click(object sender, EventArgs e)
        {
            if (dataGridViewPasajes.SelectedRows != null)
            {
                Pasaje P = (Pasaje)dataGridViewPasajes.SelectedRows[0].DataBoundItem;
                int rto = pasajeBLL.CancelarPasaje(P);
                if (rto > 0)
                {
                    LlenarGrillaPasajes();
                }
                else if (rto == -3)
                {
                    MessageBox.Show("No se puede cancelar este viaje\n\nDias minimos para cancelar: " + pasajeBLL.fechaLimite.ToString());
                }
            }
        }
        #endregion

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            LlenarGrillaAviones();
        }
        
        private void CommitQuery_Click(object sender, EventArgs e)
        {
            int cont = -1;
            string SQL = " select P.*, CD.Nombre as 'destino', CO.Nombre as 'origen' from Pasaje as P " +
                " inner join Ciudad as CD on CD.Id = P.CiudadDestino " +
                " inner join Ciudad as CO on CO.Id = P.CiudadOrigen ";
            string tipo;
            if (radioButtonNacinoal.Checked == true)
            {
                tipo = radioButtonNacinoal.Text;
            }
            else tipo = radioButtonnternacinoal.Text;
            if (tipo != null)
            {
                SQL += " WHERE P.Tipo = '" + tipo +"'";
                cont = 1;
            }
            if (textBoxdestino.Text != "")
            {
             //   Ciudad C = (Ciudad)comboBoxdestinos.SelectedItem.ToString().Split(',')[0];
                SQL += " AND CD.Nombre = '" + textBoxdestino.Text.Split(',')[0] + "'";
                cont = 2;
            }
            if(dateTimePicker1.Value != null)
            {
                DateTime fecha = dateTimePicker1.Value;
                SQL += " and P.FechaHoraPartida >= '" + fecha.Month.ToString() + "-"+fecha.Day.ToString() + "-"+fecha.Year.ToString()+ "'";
                cont = 3;
            }
            if (cont >= 1)
            {
                List<Pasaje> pasajes = pasajeBLL.consultaCompuesta(SQL);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = pasajes;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillComboBoxCiudades();
        }

        private void comboBoxdestinos_TextChanged(object sender, EventArgs e)
        {
            comboBoxdestinos.Items.Clear();
            try
            {
                Ciudad showC = new Ciudad();
                foreach (Ciudad C in ciudadBLL.GetCiudadesByName(comboBoxdestinos.Text))
                {
                    comboBoxdestinos.Items.Add(C.Nombre);
                    showC.Pais = C.Pais;
                }
                comboBoxdestinos.Focus();
                comboBoxdestinos.SelectionStart = comboBoxdestinos.Text.Length;

                if (comboBoxdestinos.Items.Count == 1)
                {
                    textBoxdestino.Text = comboBoxdestinos.Text + "," + showC.Pais.Nombre;
                }
                else textBoxdestino.Text = comboBoxdestinos.Text;
            }
            catch (Exception) { FillComboBoxCiudades(); }
        }

        private void buttonDepu_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Se eliminaran todos los vuelos anteriores al dia de hoy", "", MessageBoxButtons.OKCancel);
            pasajeBLL.Depurar();
            
        }

        private void buttonshowCiudadOrigen_Click(object sender, EventArgs e)
        {
            Ciudad CO = ciudadBLL.GetCiudadByName(textBoxCOrig.Text.Split(',')[0]);
            GMA.CallMaps(CO.Latitud.ToString()+","+CO.Longitud.ToString());
        }

        private void buttonshowCiudadDestino_Click(object sender, EventArgs e)
        {
            Ciudad CO = ciudadBLL.GetCiudadByName(textBoxCDest.Text.Split(',')[0]);
            GMA.CallMaps(CO.Latitud.ToString() + "," + CO.Longitud.ToString());
        }

        private void viewcityButton_Click(object sender, EventArgs e)
        {
            Ciudad CO = ciudadBLL.GetCiudadByName(textBoxcityname.Text.Split(',')[0]);
            GMA.CallMaps(CO.Latitud.ToString() + "," + CO.Longitud.ToString());
        }

        private void textBoxLat_TextChanged(object sender, EventArgs e)
        {
            if (int.Parse(textBoxLat.Text) > -91 && int.Parse(textBoxLat.Text) < 91 )
            {

            }
            else
            {
                MessageBox.Show("Latitud debe ser entre -90 y 90");
            }
        }

        private void textBoxLong_TextChanged(object sender, EventArgs e)
        {
            if (int.Parse(textBoxLong.Text) > -181 && int.Parse(textBoxLong.Text) < 181)
            {

            }
            else
            {
                MessageBox.Show("Longitud debe ser entre -180 y 180");
            }
        }
    }
}
