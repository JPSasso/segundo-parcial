﻿using BE;
using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppAeropuerto
{
    public partial class Pasaje_loader : Form
    {
        public Pasaje_loader()
        {
            InitializeComponent();
        }
        public Pasaje pasajeIda = new Pasaje();
        public Pasaje pasajevuelta = new Pasaje();
        readonly AvionBLL avionBLL = new AvionBLL();
        readonly PasajeBLL pasajeBLL = new PasajeBLL();
        readonly Escala[] Escalas = new Escala[3];
        public int numero_De_Pasajes = 0;
        public void button1_Click(object sender, EventArgs e)
        {
            button3.Visible = true;
            ConfirmE1.Visible = true;
            EscalasControler escalasControler1 = new EscalasControler
            {
                Location = new Point(428, 9)
            };
            escalasControler1.SendToBack();
            escalasControler1.pasaje = this.pasajeIda;
            this.Controls.Add(escalasControler1);
            Escalas[0] = escalasControler1.escala;
        }


        private void button3_Click_1(object sender, EventArgs e)
        {
            ConfirmE2.Visible = true;
            button5.Visible = true;
            EscalasControler escalasControler2 = new EscalasControler
            {
                Location = new Point(431, 277)
            };
            escalasControler2.SendToBack();
            this.Controls.Add(escalasControler2);
            Escalas[1] = escalasControler2.escala;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ConfirmE3.Visible = true;
            EscalasControler escalasControler3 = new EscalasControler
            {
                Location = new Point(434, 545)
            };
            escalasControler3.SendToBack();
            this.Controls.Add(escalasControler3);
            Escalas[2] = escalasControler3.escala;
        }      

        private void Pasaje_loader_Load(object sender, EventArgs e)
        {
            labelNumber.Text = pasajeIda.NumeroPasaje.ToString();
            labelPasajero.Text = pasajeIda.Pasajero.Apellido + "," + pasajeIda.Pasajero.Nombre;
            labelAvion.Text = pasajeIda.Avion.Modelo;
            labelciudadOrigen.Text = pasajeIda._cOrigen.Nombre + "," + pasajeIda._cOrigen.Pais.Nombre;
            labelciudadDestino.Text = pasajeIda._cDestino.Nombre + "," + pasajeIda._cDestino.Pais.Nombre;
            labelTipo.Text = pasajeIda.Tipo;
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            pasajeIda = null;
            this.Close();
        }

        private void confirmbutton_Click(object sender, EventArgs e)
        {
            pasajeIda.Avion.Pasajeros.Add(pasajeIda.Pasajero);            
            avionBLL.ModificarAvion(pasajeIda.Avion);
            pasajeIda.Precio = float.Parse( textBoxTotaPrice.Text);
            if (pasajevuelta != null)
            {
                pasajevuelta.Avion.Pasajeros.Add(pasajevuelta.Pasajero);
                avionBLL.ModificarAvion(pasajevuelta.Avion);
                pasajevuelta.Precio = float.Parse(textBoxTotaPrice.Text);
            }
            this.Close();
        }
        private void ActualizarHoras()
        {
            try
            {
                DateTime horaSalida = DateTime.Parse(labelFechaHoraOrigen.Text);
                DateTime horaLlegada = horaSalida.AddHours(double.Parse(textBoxHorasVuelo.Value.ToString()));
                
                labelFechaHoraOrigen.Text = pasajeIda.FechaOrigen.ToString("dd/MM/yyyy ") + comboBoxHorasSalida.Text;
                labelFechaHoraDestino.Text = horaLlegada.ToString("dd/MM/yyyy HH:mm");
                
                pasajeIda.FechaOrigen = DateTime.Parse(labelFechaHoraOrigen.Text);
                pasajeIda.FechaLlegada = DateTime.Parse(labelFechaHoraDestino.Text);
                if (pasajevuelta != null)
                {
                    pasajevuelta.FechaOrigen = DateTime.Parse(pasajevuelta.FechaOrigen.ToString("dd/MM/yyyy ") + comboBoxHorasSalida.Text);
                    pasajevuelta.FechaLlegada = pasajevuelta.FechaOrigen.AddHours(double.Parse(textBoxHorasVuelo.Value.ToString()));
                }
            }
            catch (Exception) { }
        }

        private void comboBoxHorasSalida_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelFechaHoraOrigen.Text = pasajeIda.FechaOrigen.ToString("dd/MM/yyyy ") + comboBoxHorasSalida.Text;
            ActualizarHoras();
        }        
        private void textBoxHorasVuelo_SelectedItemChanged(object sender, EventArgs e)
        {
            ActualizarHoras();
        }
        private void ConfirmE1_Click(object sender, EventArgs e)
        { 
            int rto = pasajeBLL.AñadirEscalasAVuelo(pasajeIda, Escalas[0]);
            if (pasajevuelta != null)
            {
                pasajeBLL.AñadirEscalasAVuelo(pasajevuelta, Escalas[0]);
            }
            if (rto > 0)
            {
                MessageBox.Show("Escala " + pasajeIda._cOrigen + "-" + Escalas[0].CiudadEscala + " confirmada");
                Escalas[0] = null;
                try
                {
                    textBoxTotaPrice.Text = (int.Parse(textBoxTotaPrice.Text) - (int.Parse(textBoxPrice.Text) * 0.1)).ToString();
                }
                catch (Exception)
                {

                }
            }
            else if (rto == -3)
            {
                MessageBox.Show("Fecha Invalida!");
            }
            else if (rto == -4)
            {
                MessageBox.Show("ciudad Invalida!");
            }
            else if (rto == -5)
            {
                MessageBox.Show("Hora Invalida!");
            }
        }
        private void ConfirmE2_Click(object sender, EventArgs e)
        {
            int rto = pasajeBLL.AñadirEscalasAVuelo(pasajeIda, Escalas[1]);
            pasajeBLL.AñadirEscalasAVuelo(pasajevuelta, Escalas[2]);
            if (rto > 0)
            {
                MessageBox.Show("Escala " + pasajeIda._cOrigen + "-" + Escalas[1].CiudadEscala + " confirmada");
                Escalas[1] = null;
                textBoxTotaPrice.Text = (int.Parse(textBoxTotaPrice.Text) - (int.Parse(textBoxPrice.Text) * 0.1)).ToString();
            }
            else if (rto == -3)
            {
                MessageBox.Show("Fecha Invalida!");
            }
            else if (rto == -4)
            {
                MessageBox.Show("ciudad Invalida!");
            }
            else if (rto == -5)
            {
                MessageBox.Show("Hora Invalida!");
            }
        }
        private void ConfirmE3_Click(object sender, EventArgs e)
        { 
            int rto = pasajeBLL.AñadirEscalasAVuelo(pasajeIda, Escalas[2]);
            pasajeBLL.AñadirEscalasAVuelo(pasajevuelta, Escalas[2]);
            if (rto > 0)
            {
                MessageBox.Show("Escala " + pasajeIda._cOrigen + "-" + Escalas[2].CiudadEscala + " confirmada");
                Escalas[2] = null; 
                textBoxTotaPrice.Text = (int.Parse(textBoxTotaPrice.Text) - (int.Parse(textBoxPrice.Text) * 0.1)).ToString();
            }
            else if (rto == -3)
            {
                MessageBox.Show("Fecha Invalida!");
            }
            else if (rto == -4)
            {
                MessageBox.Show("ciudad Invalida!");
            }
            else if (rto == -5)
            {
                MessageBox.Show("Hora Invalida!");
            }
        }
        private void textBoxPrice_TextChanged(object sender, EventArgs e)
        {
            textBoxTotaPrice.Text = textBoxPrice.Text;
        }               
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            ActualizarHoras();
        }
    }
}
