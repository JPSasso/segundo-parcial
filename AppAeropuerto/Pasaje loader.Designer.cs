﻿namespace AppAeropuerto
{
    partial class Pasaje_loader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelNumber = new System.Windows.Forms.Label();
            this.labelPasajero = new System.Windows.Forms.Label();
            this.labelAvion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelciudadOrigen = new System.Windows.Forms.Label();
            this.labelFechaHoraOrigen = new System.Windows.Forms.Label();
            this.labelFechaHoraDestino = new System.Windows.Forms.Label();
            this.labelciudadDestino = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelTipo = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ConfirmE1 = new System.Windows.Forms.Button();
            this.ConfirmE2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ConfirmE3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxHorasSalida = new System.Windows.Forms.ComboBox();
            this.Precio = new System.Windows.Forms.Label();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.textBoxTotaPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.confirmbutton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxHorasVuelo = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxHorasVuelo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Confirmacion de Pasajes";
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelNumber.Location = new System.Drawing.Point(12, 73);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(136, 18);
            this.labelNumber.TabIndex = 2;
            this.labelNumber.Text = "NumeroDePasaje";
            // 
            // labelPasajero
            // 
            this.labelPasajero.AutoSize = true;
            this.labelPasajero.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPasajero.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelPasajero.Location = new System.Drawing.Point(12, 121);
            this.labelPasajero.Name = "labelPasajero";
            this.labelPasajero.Size = new System.Drawing.Size(71, 18);
            this.labelPasajero.TabIndex = 3;
            this.labelPasajero.Text = "Pasajero";
            // 
            // labelAvion
            // 
            this.labelAvion.AutoSize = true;
            this.labelAvion.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAvion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelAvion.Location = new System.Drawing.Point(12, 176);
            this.labelAvion.Name = "labelAvion";
            this.labelAvion.Size = new System.Drawing.Size(50, 18);
            this.labelAvion.TabIndex = 4;
            this.labelAvion.Text = "Avion";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(12, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Origen";
            // 
            // labelciudadOrigen
            // 
            this.labelciudadOrigen.AutoSize = true;
            this.labelciudadOrigen.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelciudadOrigen.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelciudadOrigen.Location = new System.Drawing.Point(12, 257);
            this.labelciudadOrigen.Name = "labelciudadOrigen";
            this.labelciudadOrigen.Size = new System.Drawing.Size(116, 18);
            this.labelciudadOrigen.TabIndex = 6;
            this.labelciudadOrigen.Text = "ciudad Origen";
            // 
            // labelFechaHoraOrigen
            // 
            this.labelFechaHoraOrigen.AutoSize = true;
            this.labelFechaHoraOrigen.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFechaHoraOrigen.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelFechaHoraOrigen.Location = new System.Drawing.Point(9, 338);
            this.labelFechaHoraOrigen.Name = "labelFechaHoraOrigen";
            this.labelFechaHoraOrigen.Size = new System.Drawing.Size(147, 18);
            this.labelFechaHoraOrigen.TabIndex = 7;
            this.labelFechaHoraOrigen.Text = "Fecha/HoraOrigen";
            // 
            // labelFechaHoraDestino
            // 
            this.labelFechaHoraDestino.AutoSize = true;
            this.labelFechaHoraDestino.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFechaHoraDestino.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelFechaHoraDestino.Location = new System.Drawing.Point(12, 459);
            this.labelFechaHoraDestino.Name = "labelFechaHoraDestino";
            this.labelFechaHoraDestino.Size = new System.Drawing.Size(150, 18);
            this.labelFechaHoraDestino.TabIndex = 10;
            this.labelFechaHoraDestino.Text = "Fecha/HoraDestino";
            // 
            // labelciudadDestino
            // 
            this.labelciudadDestino.AutoSize = true;
            this.labelciudadDestino.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelciudadDestino.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelciudadDestino.Location = new System.Drawing.Point(12, 428);
            this.labelciudadDestino.Name = "labelciudadDestino";
            this.labelciudadDestino.Size = new System.Drawing.Size(119, 18);
            this.labelciudadDestino.TabIndex = 9;
            this.labelciudadDestino.Text = "ciudad Destino";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(12, 397);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Destino";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTipo
            // 
            this.labelTipo.AutoSize = true;
            this.labelTipo.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelTipo.Location = new System.Drawing.Point(12, 497);
            this.labelTipo.Name = "labelTipo";
            this.labelTipo.Size = new System.Drawing.Size(36, 18);
            this.labelTipo.TabIndex = 11;
            this.labelTipo.Text = "tipo";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkOrange;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(431, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 32);
            this.button1.TabIndex = 15;
            this.button1.Tag = "1";
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ConfirmE1
            // 
            this.ConfirmE1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ConfirmE1.BackColor = System.Drawing.Color.DarkOrange;
            this.ConfirmE1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ConfirmE1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.ConfirmE1.Location = new System.Drawing.Point(468, 244);
            this.ConfirmE1.Name = "ConfirmE1";
            this.ConfirmE1.Size = new System.Drawing.Size(182, 27);
            this.ConfirmE1.TabIndex = 44;
            this.ConfirmE1.Tag = "1";
            this.ConfirmE1.Text = "Confirmar Escala";
            this.ConfirmE1.UseVisualStyleBackColor = false;
            this.ConfirmE1.Visible = false;
            this.ConfirmE1.Click += new System.EventHandler(this.ConfirmE1_Click);
            // 
            // ConfirmE2
            // 
            this.ConfirmE2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ConfirmE2.BackColor = System.Drawing.Color.DarkOrange;
            this.ConfirmE2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ConfirmE2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.ConfirmE2.Location = new System.Drawing.Point(471, 512);
            this.ConfirmE2.Name = "ConfirmE2";
            this.ConfirmE2.Size = new System.Drawing.Size(182, 27);
            this.ConfirmE2.TabIndex = 47;
            this.ConfirmE2.Tag = "1";
            this.ConfirmE2.Text = "Confirmar Escala";
            this.ConfirmE2.UseVisualStyleBackColor = false;
            this.ConfirmE2.Visible = false;
            this.ConfirmE2.Click += new System.EventHandler(this.ConfirmE2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkOrange;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(434, 280);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 32);
            this.button3.TabIndex = 46;
            this.button3.Tag = "1";
            this.button3.Text = "+";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // ConfirmE3
            // 
            this.ConfirmE3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ConfirmE3.BackColor = System.Drawing.Color.DarkOrange;
            this.ConfirmE3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ConfirmE3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.ConfirmE3.Location = new System.Drawing.Point(474, 780);
            this.ConfirmE3.Name = "ConfirmE3";
            this.ConfirmE3.Size = new System.Drawing.Size(182, 27);
            this.ConfirmE3.TabIndex = 50;
            this.ConfirmE3.Tag = "1";
            this.ConfirmE3.Text = "Confirmar Escala";
            this.ConfirmE3.UseVisualStyleBackColor = false;
            this.ConfirmE3.Visible = false;
            this.ConfirmE3.Click += new System.EventHandler(this.ConfirmE3_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkOrange;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.button5.Location = new System.Drawing.Point(437, 548);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 32);
            this.button5.TabIndex = 49;
            this.button5.Tag = "1";
            this.button5.Text = "+";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(165, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 18);
            this.label3.TabIndex = 54;
            this.label3.Tag = "1";
            this.label3.Text = "Horas de Vuelo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(9, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 18);
            this.label4.TabIndex = 53;
            this.label4.Tag = "1";
            this.label4.Text = "Hora de salida";
            // 
            // comboBoxHorasSalida
            // 
            this.comboBoxHorasSalida.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxHorasSalida.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxHorasSalida.FormattingEnabled = true;
            this.comboBoxHorasSalida.Items.AddRange(new object[] {
            "05:00",
            "07:30",
            "09:00",
            "10:45",
            "12:00",
            "14:30",
            "16:15",
            "18:00",
            "21:10",
            "23:25"});
            this.comboBoxHorasSalida.Location = new System.Drawing.Point(9, 306);
            this.comboBoxHorasSalida.MaxDropDownItems = 10;
            this.comboBoxHorasSalida.Name = "comboBoxHorasSalida";
            this.comboBoxHorasSalida.Size = new System.Drawing.Size(139, 29);
            this.comboBoxHorasSalida.Sorted = true;
            this.comboBoxHorasSalida.TabIndex = 52;
            this.comboBoxHorasSalida.Tag = "1";
            this.comboBoxHorasSalida.SelectedIndexChanged += new System.EventHandler(this.comboBoxHorasSalida_SelectedIndexChanged);
            // 
            // Precio
            // 
            this.Precio.AutoSize = true;
            this.Precio.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Precio.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Precio.Location = new System.Drawing.Point(12, 545);
            this.Precio.Name = "Precio";
            this.Precio.Size = new System.Drawing.Size(55, 18);
            this.Precio.TabIndex = 56;
            this.Precio.Text = "Precio";
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxPrice.Location = new System.Drawing.Point(12, 566);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(150, 27);
            this.textBoxPrice.TabIndex = 57;
            this.textBoxPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxPrice.TextChanged += new System.EventHandler(this.textBoxPrice_TextChanged);
            // 
            // textBoxTotaPrice
            // 
            this.textBoxTotaPrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxTotaPrice.Location = new System.Drawing.Point(12, 629);
            this.textBoxTotaPrice.Name = "textBoxTotaPrice";
            this.textBoxTotaPrice.Size = new System.Drawing.Size(150, 27);
            this.textBoxTotaPrice.TabIndex = 59;
            this.textBoxTotaPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(13, 608);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 18);
            this.label6.TabIndex = 58;
            this.label6.Text = "Total";
            // 
            // cancelButton
            // 
            this.cancelButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cancelButton.BackColor = System.Drawing.Color.DarkOrange;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.cancelButton.Location = new System.Drawing.Point(9, 736);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(169, 83);
            this.cancelButton.TabIndex = 60;
            this.cancelButton.Tag = "1";
            this.cancelButton.Text = "CANCELAR";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // confirmbutton
            // 
            this.confirmbutton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.confirmbutton.BackColor = System.Drawing.Color.DarkOrange;
            this.confirmbutton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.confirmbutton.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.confirmbutton.Location = new System.Drawing.Point(242, 736);
            this.confirmbutton.Name = "confirmbutton";
            this.confirmbutton.Size = new System.Drawing.Size(169, 83);
            this.confirmbutton.TabIndex = 61;
            this.confirmbutton.Tag = "1";
            this.confirmbutton.Text = "CONFIRMAR";
            this.confirmbutton.UseVisualStyleBackColor = false;
            this.confirmbutton.Click += new System.EventHandler(this.confirmbutton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(12, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 18);
            this.label7.TabIndex = 62;
            this.label7.Text = "NumeroDePasaje";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(13, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 18);
            this.label8.TabIndex = 63;
            this.label8.Text = "Pasajero";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(12, 158);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 18);
            this.label9.TabIndex = 64;
            this.label9.Text = "Avion";
            // 
            // textBoxHorasVuelo
            // 
            this.textBoxHorasVuelo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxHorasVuelo.Location = new System.Drawing.Point(292, 308);
            this.textBoxHorasVuelo.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.textBoxHorasVuelo.Name = "textBoxHorasVuelo";
            this.textBoxHorasVuelo.Size = new System.Drawing.Size(63, 27);
            this.textBoxHorasVuelo.TabIndex = 65;
            this.textBoxHorasVuelo.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // Pasaje_loader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(843, 831);
            this.Controls.Add(this.textBoxHorasVuelo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.confirmbutton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.textBoxTotaPrice);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.Precio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxHorasSalida);
            this.Controls.Add(this.ConfirmE3);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.ConfirmE2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.ConfirmE1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelTipo);
            this.Controls.Add(this.labelFechaHoraDestino);
            this.Controls.Add(this.labelciudadDestino);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelFechaHoraOrigen);
            this.Controls.Add(this.labelciudadOrigen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelAvion);
            this.Controls.Add(this.labelPasajero);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.label1);
            this.Name = "Pasaje_loader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pasaje_loader";
            this.Load += new System.EventHandler(this.Pasaje_loader_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxHorasVuelo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.Label labelPasajero;
        private System.Windows.Forms.Label labelAvion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelciudadOrigen;
        private System.Windows.Forms.Label labelFechaHoraOrigen;
        private System.Windows.Forms.Label labelFechaHoraDestino;
        private System.Windows.Forms.Label labelciudadDestino;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelTipo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ConfirmE1;
        private System.Windows.Forms.Button ConfirmE2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button ConfirmE3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxHorasSalida;
        private System.Windows.Forms.Label Precio;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.TextBox textBoxTotaPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button confirmbutton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown textBoxHorasVuelo;
    }
}