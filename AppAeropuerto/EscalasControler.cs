﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using BLL;

namespace AppAeropuerto
{
    public partial class EscalasControler : UserControl
    {
        public EscalasControler()
        {
            InitializeComponent();
        }
        private readonly CiudadBLL ciudadBLL = new CiudadBLL();
        readonly PasajeBLL pasajeBLL = new PasajeBLL();
        public DateTime fecha;// = DateTime.Today;
        public Escala escala = new Escala();
        public Pasaje pasaje = new Pasaje();
        readonly googleMapsAssistant GMA = new googleMapsAssistant();
        private void EscalasControler_Load(object sender, EventArgs e)
        {
            FillComboBoxCiudades();
            fecha = pasaje.FechaOrigen;
            labelHoraSalida.Text = fecha.ToString("dd/MM/yyyy ");
        }

        private void comboBoxCiudad_TextChanged(object sender, EventArgs e)
        {
            comboBoxCiudad.Items.Clear();
            try
            {
                Ciudad showC = new Ciudad();
                foreach (Ciudad C in ciudadBLL.GetCiudadesByName(comboBoxCiudad.Text))
                {
                    comboBoxCiudad.Items.Add(C.Nombre);
                    showC.Pais = C.Pais;
                }
                comboBoxCiudad.Focus();
                comboBoxCiudad.SelectionStart = comboBoxCiudad.Text.Length;

                if (comboBoxCiudad.Items.Count == 1)
                {
                    escala.CiudadEscala = ciudadBLL.GetCiudadByName(comboBoxCiudad.Text);
                    textBoxCiudad.Text = comboBoxCiudad.Text + "," + showC.Pais.Nombre;
                }
                else textBoxCiudad.Text = comboBoxCiudad.Text;
            }
            catch (Exception) { FillComboBoxCiudades(); }
        }

        private void FillComboBoxCiudades()
        {
            comboBoxCiudad.Items.Clear();
            foreach (Ciudad c in ciudadBLL.ListarCiudades())
            {
                comboBoxCiudad.Items.Add(c.Nombre);
            }
        }


        private void ActualizarHoras()
        {
            try
            {
                DateTime horaSalida = DateTime.Parse(labelHoraSalida.Text);
                DateTime horaLlegada = horaSalida.AddHours(double.Parse(comboBoxHorasVuelo.Value.ToString()));

                labelHoraSalida.Text = fecha.ToString("dd/MM/yyyy ") + comboBoxHorasSalida.SelectedItem.ToString();
                labelHoraLlegada.Text = horaLlegada.ToString("dd/MM/yyyy HH:mm");

                escala.FechaOrigen = DateTime.Parse(labelHoraSalida.Text);
                escala.FechaLlegada = DateTime.Parse(labelHoraLlegada.Text);
            }
            catch (Exception) { }
        }

        private void comboBoxCiudad_SelectedValueChanged(object sender, EventArgs e)
        {
            ActualizarHoras();            
        }

        private void comboBoxHorasSalida_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelHoraSalida.Text = pasaje.FechaOrigen.ToString("dd/MM/yyyy ") + comboBoxHorasSalida.Text;
            ActualizarHoras();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            pasajeBLL.QuitarEscala(pasaje,escala);
            this.Dispose();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                labelHoraLlegada.Text = fecha.Date.ToString() + " " + comboBoxHorasSalida.SelectedItem.ToString();
            }
            catch (Exception)
            {
            }
            ActualizarHoras();
        }

        private void buttonshowCiudad_Click(object sender, EventArgs e)
        {
            Ciudad CO = ciudadBLL.GetCiudadByName(textBoxCiudad.Text.Split(',')[0]);
            GMA.CallMaps(CO.Latitud.ToString() + "," + CO.Longitud.ToString());
        }
    }
}
