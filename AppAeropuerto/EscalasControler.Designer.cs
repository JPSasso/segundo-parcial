﻿namespace AppAeropuerto
{
    partial class EscalasControler
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxCiudad = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonshowCiudad = new System.Windows.Forms.Button();
            this.textBoxCiudad = new System.Windows.Forms.TextBox();
            this.comboBoxHorasSalida = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelHoraSalida = new System.Windows.Forms.Label();
            this.labelHoraLlegada = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.comboBoxHorasVuelo = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxHorasVuelo)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxCiudad
            // 
            this.comboBoxCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxCiudad.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCiudad.FormattingEnabled = true;
            this.comboBoxCiudad.Location = new System.Drawing.Point(54, 27);
            this.comboBoxCiudad.MaxDropDownItems = 10;
            this.comboBoxCiudad.Name = "comboBoxCiudad";
            this.comboBoxCiudad.Size = new System.Drawing.Size(250, 29);
            this.comboBoxCiudad.Sorted = true;
            this.comboBoxCiudad.TabIndex = 15;
            this.comboBoxCiudad.Tag = "1";
            this.comboBoxCiudad.SelectedValueChanged += new System.EventHandler(this.comboBoxCiudad_SelectedValueChanged);
            this.comboBoxCiudad.TextChanged += new System.EventHandler(this.comboBoxCiudad_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(54, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 18);
            this.label8.TabIndex = 16;
            this.label8.Tag = "1";
            this.label8.Text = "Escala";
            // 
            // buttonshowCiudad
            // 
            this.buttonshowCiudad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonshowCiudad.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonshowCiudad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonshowCiudad.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.buttonshowCiudad.Location = new System.Drawing.Point(310, 73);
            this.buttonshowCiudad.Name = "buttonshowCiudad";
            this.buttonshowCiudad.Size = new System.Drawing.Size(82, 27);
            this.buttonshowCiudad.TabIndex = 36;
            this.buttonshowCiudad.Tag = "1";
            this.buttonshowCiudad.Text = "Ver Mapa";
            this.buttonshowCiudad.UseVisualStyleBackColor = false;
            this.buttonshowCiudad.Click += new System.EventHandler(this.buttonshowCiudad_Click);
            // 
            // textBoxCiudad
            // 
            this.textBoxCiudad.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxCiudad.Location = new System.Drawing.Point(54, 73);
            this.textBoxCiudad.Name = "textBoxCiudad";
            this.textBoxCiudad.Size = new System.Drawing.Size(250, 27);
            this.textBoxCiudad.TabIndex = 35;
            this.textBoxCiudad.Tag = "1";
            // 
            // comboBoxHorasSalida
            // 
            this.comboBoxHorasSalida.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxHorasSalida.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxHorasSalida.FormattingEnabled = true;
            this.comboBoxHorasSalida.Items.AddRange(new object[] {
            "05:00",
            "07:30",
            "09:00",
            "10:45",
            "12:00",
            "14:30",
            "16:15",
            "18:00",
            "21:10",
            "23:25"});
            this.comboBoxHorasSalida.Location = new System.Drawing.Point(54, 121);
            this.comboBoxHorasSalida.MaxDropDownItems = 10;
            this.comboBoxHorasSalida.Name = "comboBoxHorasSalida";
            this.comboBoxHorasSalida.Size = new System.Drawing.Size(139, 29);
            this.comboBoxHorasSalida.Sorted = true;
            this.comboBoxHorasSalida.TabIndex = 37;
            this.comboBoxHorasSalida.Tag = "1";
            this.comboBoxHorasSalida.SelectedIndexChanged += new System.EventHandler(this.comboBoxHorasSalida_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(54, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 18);
            this.label1.TabIndex = 38;
            this.label1.Tag = "1";
            this.label1.Text = "Hora de salida";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(210, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 18);
            this.label2.TabIndex = 40;
            this.label2.Tag = "1";
            this.label2.Text = "Horas de Vuelo";
            // 
            // labelHoraSalida
            // 
            this.labelHoraSalida.AutoSize = true;
            this.labelHoraSalida.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHoraSalida.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelHoraSalida.Location = new System.Drawing.Point(54, 191);
            this.labelHoraSalida.Name = "labelHoraSalida";
            this.labelHoraSalida.Size = new System.Drawing.Size(115, 18);
            this.labelHoraSalida.TabIndex = 41;
            this.labelHoraSalida.Tag = "1";
            this.labelHoraSalida.Text = "Hora de salida";
            // 
            // labelHoraLlegada
            // 
            this.labelHoraLlegada.AutoSize = true;
            this.labelHoraLlegada.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHoraLlegada.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelHoraLlegada.Location = new System.Drawing.Point(240, 191);
            this.labelHoraLlegada.Name = "labelHoraLlegada";
            this.labelHoraLlegada.Size = new System.Drawing.Size(132, 18);
            this.labelHoraLlegada.TabIndex = 42;
            this.labelHoraLlegada.Tag = "1";
            this.labelHoraLlegada.Text = "Hora de Llegada";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkOrange;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(3, 44);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 32);
            this.button4.TabIndex = 44;
            this.button4.Tag = "4";
            this.button4.Text = "-";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // comboBoxHorasVuelo
            // 
            this.comboBoxHorasVuelo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.comboBoxHorasVuelo.Location = new System.Drawing.Point(337, 121);
            this.comboBoxHorasVuelo.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.comboBoxHorasVuelo.Name = "comboBoxHorasVuelo";
            this.comboBoxHorasVuelo.Size = new System.Drawing.Size(63, 27);
            this.comboBoxHorasVuelo.TabIndex = 46;
            this.comboBoxHorasVuelo.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // EscalasControler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.Controls.Add(this.comboBoxHorasVuelo);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelHoraLlegada);
            this.Controls.Add(this.labelHoraSalida);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxHorasSalida);
            this.Controls.Add(this.buttonshowCiudad);
            this.Controls.Add(this.textBoxCiudad);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxCiudad);
            this.Name = "EscalasControler";
            this.Size = new System.Drawing.Size(403, 262);
            this.Load += new System.EventHandler(this.EscalasControler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxHorasVuelo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxCiudad;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonshowCiudad;
        private System.Windows.Forms.TextBox textBoxCiudad;
        private System.Windows.Forms.ComboBox comboBoxHorasSalida;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelHoraSalida;
        private System.Windows.Forms.Label labelHoraLlegada;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.NumericUpDown comboBoxHorasVuelo;
    }
}
