USE [master]
GO

/****** Object:  Database [Batallanaval]    Script Date: 17/11/2020 23:28:40 ******/
CREATE DATABASE [Batallanaval]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Batallanaval', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Batallanaval.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Batallanaval_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Batallanaval_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [Batallanaval] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Batallanaval].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Batallanaval] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [Batallanaval] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [Batallanaval] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [Batallanaval] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [Batallanaval] SET ARITHABORT OFF 
GO

ALTER DATABASE [Batallanaval] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [Batallanaval] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [Batallanaval] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [Batallanaval] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [Batallanaval] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [Batallanaval] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [Batallanaval] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [Batallanaval] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [Batallanaval] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [Batallanaval] SET  DISABLE_BROKER 
GO

ALTER DATABASE [Batallanaval] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [Batallanaval] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [Batallanaval] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [Batallanaval] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO

ALTER DATABASE [Batallanaval] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [Batallanaval] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [Batallanaval] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [Batallanaval] SET RECOVERY FULL 
GO

ALTER DATABASE [Batallanaval] SET  MULTI_USER 
GO

ALTER DATABASE [Batallanaval] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [Batallanaval] SET DB_CHAINING OFF 
GO

ALTER DATABASE [Batallanaval] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [Batallanaval] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [Batallanaval] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [Batallanaval] SET QUERY_STORE = OFF
GO

USE [Batallanaval]
GO

ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

ALTER DATABASE [Batallanaval] SET  READ_WRITE 
GO

USE [Batallanaval]
GO

/****** Object:  Table [dbo].[Bitacora]    Script Date: 18/11/2020 19:13:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bitacora](
	[PKey] [varchar](50) NOT NULL,
	[Tipomsg] [varchar](50) NULL,
	[Mensaje] [varchar](50) NULL,
 CONSTRAINT [PK_Bitacora] PRIMARY KEY CLUSTERED 
(
	[PKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Batallanaval]
GO

/****** Object:  Table [dbo].[Usuario]    Script Date: 18/11/2020 19:13:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Usuario](
	[ID] [int] NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Victorias] [int] NULL,
	[Derrotas] [int] NULL,
	[Empates] [int] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[BuscarUsuario]    Script Date: 18/11/2020 19:14:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[BuscarUsuario]
@user varchar(50)
as
begin 
	SELECT [ID]
      ,[Username]
      ,[Password]
      ,[Victorias]
      ,[Derrotas]
      ,[Empates]
  FROM [dbo].[Usuario]
  where Username = @user
end
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[DoBackUp]    Script Date: 18/11/2020 19:14:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DoBackUp]
AS
BEGIN
BACKUP DATABASE [Batallanaval] TO  DISK = N'C:\BattleShip\Backup\Batallanaval.bak' 
WITH NOFORMAT, NOINIT,  NAME = N'Batallanaval-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
END
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[EliminarUsuario]    Script Date: 18/11/2020 19:14:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[EliminarUsuario]
@user varchar(50)
as
begin 
DELETE FROM [dbo].[Usuario]
      WHERE Username = @user
end
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[InsertarBitacora]    Script Date: 18/11/2020 19:14:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[InsertarBitacora]
@key varchar(50), @tipo varchar(50), @mensaje varchar(50)
as
begin
INSERT INTO [dbo].[Bitacora]
           ([PKey]
           ,[Tipomsg]
           ,[Mensaje])
     VALUES
           (@key,@tipo, @mensaje)
end
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[InsertarUsuario]    Script Date: 18/11/2020 19:14:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[InsertarUsuario]
@user varchar(50), @pw varchar(50)
as
begin 
declare @id int
	set @id = (select ISNULL(MAX(ID),0)+1 from Usuario)
INSERT INTO [dbo].[Usuario]
           ([ID],[Username] ,[Password] ,[Victorias] ,[Derrotas] ,[Empates])
     VALUES
           (@id,@user, @pw, 0 ,0 ,0)
end
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[ListarUsuarios]    Script Date: 18/11/2020 19:14:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[ListarUsuarios]
as
begin 
	SELECT [ID]
      ,[Username]
      ,[Password]
      ,[Victorias]
      ,[Derrotas]
      ,[Empates]
  FROM [dbo].[Usuario]
end
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[RestoreDB]    Script Date: 18/11/2020 19:15:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[RestoreDB]
as
begin
RESTORE FILELISTONLY FROM DISK = N'C:\Backup\BattleShip\Batallanaval.bak'

----Make Database to single user Mode
ALTER DATABASE Batallanaval
SET SINGLE_USER WITH
ROLLBACK IMMEDIATE
 
----Restore Database
RESTORE DATABASE Batallanaval
FROM DISK = 'C:\Backup\BattleShip\Batallanaval.bak'
WITH MOVE 'BatallanavalMDF' TO 'C:\Backup\BattleShip\DataBatallanavalMDF.mdf',
MOVE 'BatallanavalLDF' TO 'C:\Backup\BattleShip\DataBatallanavalLDF.ldf'
 
/*If there is no error in statement before database will be in multiuser
mode.
If error occurs please execute following command it will convert
database in multi user.*/
ALTER DATABASE Batallanaval SET MULTI_USER
end
GO

USE [Batallanaval]
GO

/****** Object:  StoredProcedure [dbo].[SetPoints]    Script Date: 18/11/2020 19:15:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create proc [dbo].[SetPoints]
@user varchar(50), @V int = 0, @D int = 0, @E int = 0
as
begin 
UPDATE [dbo].[Usuario]
   SET [Victorias] = @V
      ,[Derrotas] = @D
      ,[Empates] = @E
 WHERE Username = @user
end
GO

