﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BE
{
    public class Avion
    {
        private int patente;
        public int Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private string modelo;
        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        private int capacidad;        
        public int Capacidad
        {
            get { return capacidad; }
            set { capacidad = value; }
        }

        public List<Pasajero> Pasajeros;
        private int ocup;

        public int Ocupacion
        {
            get { return GetOcupacion(); }
            set { ocup = SetOcupacion(); }
        }

        private int SetOcupacion()
        {
            return ((Pasajeros.Count * 100)/ capacidad);
        }

        public Avion()
        {
            this.Pasajeros = new List<Pasajero>();
            this.Patente = 0;
            this.Modelo = "747";
            this.capacidad = 100;
            this.Ocupacion = 0;
        }

        public int GetOcupacion()
        {
            int pasajerosActuales = this.Pasajeros.Count();            
            if (pasajerosActuales < this.capacidad)
            {
                return (pasajerosActuales * 100) / capacidad;
            }
            else return -1;
        }
    }
}
