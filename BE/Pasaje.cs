﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Pasaje
    {
        private int numero;

        public int NumeroPasaje
        {
            get { return numero; }
            set { numero = value; }
        }
        
        private Avion avion;

        public Avion Avion
        {
            get { return avion; }
            set { avion = value; }
        }
        private Pasajero pasajero;

        public Pasajero Pasajero
        {
            get { return pasajero; }
            set { pasajero = value; }
        }

        private Ciudad ciudad;

        public Ciudad _cOrigen
        {
            get { return ciudad; }
            set { ciudad = value; }
        }
        private Ciudad ciudadd;

        public Ciudad _cDestino
        {
            get { return ciudadd; }
            set { ciudadd = value; }
        }

        private DateTime fOrig;

        public DateTime FechaOrigen
        {
            get { return fOrig; }
            set { fOrig = value; }
        }
        private DateTime fDest;

        public DateTime FechaLlegada
        {
            get { return fDest; }
            set { fDest = value; }
        }
        private float precio;

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        public List<Escala> Escalas;
        private string nacional;

        public string Tipo
        {
            get { return nacional; }
            set { nacional = value; }
        }

        public Pasaje()
        {
            this.NumeroPasaje = 1000000;
            this.Escalas = new List<Escala>();
            this.FechaOrigen = DateTime.Now.AddDays(7);
            this.FechaLlegada = this.FechaOrigen.AddHours(10);
            this.Precio = 10000f;
            this.Pasajero = null;
            this.avion = null;
            this.Tipo = "Nacional";
        }
    }
}
