﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Ciudad
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private Pais pais;

        public Pais Pais
        {
            get { return pais; }
            set { pais = value; }
        }
        private float latitud;

        public float Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }
        private float longitud;

        public float Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }

        public override string ToString()
        {
            return this.pais.Nombre + "," + this.Nombre;
        }
    }
}
