﻿using System.Collections.Generic;

namespace BE
{
    public class Pais
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public List<Ciudad> Ciudades = new List<Ciudad>();

        public Pais()
        {
            this.nombre = "";
            this.Ciudades = new List<Ciudad>();
        }

    }
}