﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Escala
    {
        private Ciudad ciudad;
        public Ciudad CiudadEscala
        {
            get { return ciudad; }
            set { ciudad = value; }
        }
        private DateTime fOrig;

        public DateTime FechaOrigen
        {
            get { return fOrig; }
            set { fOrig = value; }
        }
        private DateTime fDest;

        public DateTime FechaLlegada
        {
            get { return fDest; }
            set { fDest = value; }
        }
    }
}
