﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CiudadBLL
    {
        readonly CiudadDAL ciudadDAL = new CiudadDAL(); 
        public int AltaCiudad(Ciudad ciudad)
        {
            return ciudadDAL.Insertar(ciudad);
        }
        public int ModificarCiudad(Ciudad ciudad)
        {
            return ciudadDAL.Modificar(ciudad);
        }
        public int BajaCiudad(Ciudad ciudad)
        {
            return ciudadDAL.Borrar(ciudad);
        }
        public Ciudad GetCiudadByName(string nombre)
        {
            return ciudadDAL.GetCiudadByName(nombre);
        }       
        public List<Ciudad> ListarCiudades()
        {
            return ciudadDAL.GetCiudades();
        }

        public List<Ciudad> GetCiudadesByName(string nombre)
        {
            return ciudadDAL.GetCiudadesByName(nombre);
        }
    }
}
