﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PasajeroBLL
    {
        PasajeroDAL pasajeroDAL = new PasajeroDAL();
        
        public List<Pasajero> GetPasajerosWDNI(int dnifragment)
        {
            return pasajeroDAL.GetPasajerosByDNI(dnifragment);
        }
        public Pasajero GetPasajero(int dni)
        {
            return pasajeroDAL.GetPasajero(dni);
        }
        public int AltaPasajero (Pasajero pasajero)
        {
            return pasajeroDAL.Insertar(pasajero);
        }
        public int ModificarPasajero(Pasajero pasajero)
        {
            return pasajeroDAL.Modificar(pasajero);
        }
        public int BajaPasajero(Pasajero pasajero)
        {
            return pasajeroDAL.Borrar(pasajero);
        }
        public List<Pasajero> ListarPasajeros()
        {
            return pasajeroDAL.GetPasajeros();
        }
    }
}
