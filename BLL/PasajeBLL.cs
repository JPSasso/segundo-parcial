﻿using DAL;
using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PasajeBLL
    {
        readonly PasajeDAL pasajeDAL = new PasajeDAL();
        readonly EscalaDAL escalaDAL = new EscalaDAL();
        Pasaje pasaje = new Pasaje();
        public double fechaLimite; //Dias minimos para cancelar o modificar una operacion
        public int CrearNumeroDePasaje()
        {
            return pasajeDAL.CrearNumeroDePasaje();
        }
        public int CrearPasaje (Pasaje pasaje)
        {
            try
            {
                if (pasaje.Avion.GetOcupacion() < 100)
                {
                    return pasajeDAL.Insertar(pasaje);
                }
                else return -3;
            }
            catch (Exception) { return -3; }
        }
        public int CancelarPasaje (Pasaje pasaje)
        {
            int rto;
            if (pasaje.FechaOrigen >= DateTime.Today.AddDays(fechaLimite))
            {
                rto = escalaDAL.BorrarTodaslasEscalas(pasaje);
                rto = pasajeDAL.Borrar(pasaje);
            }
            else rto = -3; 
            return rto;
        }

        public int AñadirEscalasAVuelo(Pasaje pasaje, Escala escala)
        {
            int rto;
            if (pasaje != null)
            {
                if (pasaje.FechaOrigen >= DateTime.Today.AddDays(fechaLimite))
                {
                    if (pasaje._cOrigen != escala.CiudadEscala &&
                    pasaje._cDestino != escala.CiudadEscala)
                    {
                        if (escala.FechaOrigen > pasaje.FechaOrigen &&
                            escala.FechaLlegada > escala.FechaOrigen &&
                            escala.FechaLlegada < pasaje.FechaLlegada)
                        {
                            rto = escalaDAL.Insertar(pasaje, escala);
                            if (rto > 0)
                            {
                                pasaje.Escalas.Add(escala);
                            }
                        }
                        else rto = -5;
                    }
                    else rto = -4;
                }
                else rto = -3;
            }
            else rto = -6;
            return rto;
        }

        public int QuitarEscala(Pasaje pasaje, Escala escala)
        {
            if (pasaje.FechaOrigen >= DateTime.Today.AddDays(fechaLimite))
            {
                return escalaDAL.Borrar(pasaje, escala);
            }
            else return -3;
        }
        public int CambiarciudadEscala (Pasaje pasaje, Escala escala, Ciudad ciudad)
        {
            int rto;
            if (pasaje.FechaOrigen >= DateTime.Today.AddDays(fechaLimite))
            {
                if (pasaje.Escalas !=null && pasaje.Escalas.Count > 0)
                {
                    rto = escalaDAL.Modificar(pasaje, escala, ciudad);
                    if (rto > 0)
                    {
                        foreach (Escala e in pasaje.Escalas)
                        {
                            if (e.CiudadEscala == escala.CiudadEscala)
                            {
                                escala.CiudadEscala = ciudad;
                                e.CiudadEscala = ciudad;                                
                                break;
                            }
                        }
                    }
                }
                else rto = -4;
            }
            else rto = -3;
            return rto;
        }

        public List<Pasaje> ObtenerPasajes()
        {
            return pasajeDAL.GetPasajes();
        }

        public Pasaje BuscarPasaje(int NumeroDePasaje)
        {
            return pasajeDAL.GetPasaje(NumeroDePasaje);
        }

        public string GetTipoDeViaje(Ciudad cOrigen, Ciudad cDestino)
        {
            return cOrigen.Pais.Nombre == cDestino.Pais.Nombre ? "Nacional" : "Internacional";
        }

        public List<Pasaje> consultaCompuesta(string sQL)
        {
            return pasajeDAL.FiltroDinamico(sQL);
        }

        public void Depurar()
        {
            pasajeDAL.Depurar();
        }
    }
}
