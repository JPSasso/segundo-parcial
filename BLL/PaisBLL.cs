﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PaisBLL
    {
        readonly PaisDAL paisDAL = new PaisDAL(); 
        public int AltaPais(Pais pais)
        {
            return paisDAL.Insertar(pais);
        }
        public int ModificarPais(Pais pais, string nombre)
        {
            return paisDAL.Modificar(pais, nombre);
        }
        public int BajaPais(Pais pais)
        {
            return paisDAL.Borrar(pais);
        }
        public Pais GetPaisByName(string nombre)
        {
            return paisDAL.GetPaisByName(nombre);
        }       
        public List<Pais> ListarPaises()
        {
            return paisDAL.GetPaises();
        }
    }
}
