﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AvionBLL
    {
        AvionDAL avionDAL = new AvionDAL(); 
        public int AltaAvion(Avion avion)
        {
            return avionDAL.Insertar(avion);
        }
        public int ModificarAvion(Avion avion)
        {
            return avionDAL.Modificar(avion);
        }
        public int BajaAvion(Avion avion)
        {
            return avionDAL.Borrar(avion);
        }
        public Avion GetAvionWPatente(int patente)
        {
            return avionDAL.GetAvion(patente);
        }
        public Avion GetAvionWModel(string nombre)
        {
            return avionDAL.GetAvion(nombre);
        }
        public List<Avion> ListarAviones()
        {
            return avionDAL.GetAviones();
        }
        public void ListarPasajeros(Avion avion)
        {
            avion.Pasajeros = avionDAL.GetPasajeros(avion);
        }
    }
}
