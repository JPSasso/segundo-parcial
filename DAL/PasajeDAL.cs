﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PasajeDAL
    {
        CiudadDAL ciudadDAL = new CiudadDAL();
        AvionDAL avionDAL = new AvionDAL();
        PasajeroDAL pasajeroDAL = new PasajeroDAL();
        public int Insertar(Pasaje pasaje)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>();
            if (pasaje.FechaLlegada == DateTime.Parse("10/10/1000"))
            {
                parameters = new List<SqlParameter>
                {
                    // @boleto int, @avion int, @dni int, @ciudado int, @ciudadd int, 
                    // @fp datetime, @fl datetime, @precio float, @tipo varchar(50)
                    acceso.CrearParametro("@boleto", pasaje.NumeroPasaje),
                    acceso.CrearParametro("@avion", pasaje.Avion.Patente),
                    acceso.CrearParametro("@dni", pasaje.Pasajero.DNI),
                    acceso.CrearParametro("@ciudado", ciudadDAL.GetCiudadId(pasaje._cOrigen.Nombre)),
                    acceso.CrearParametro("@ciudadd", ciudadDAL.GetCiudadId(pasaje._cDestino.Nombre)),
                    acceso.CrearParametro("@fp", pasaje.FechaOrigen),
                    acceso.CrearParametro("@fl", null),
                    acceso.CrearParametro("@precio", pasaje.Precio),
                    acceso.CrearParametro("@tipo", pasaje.Tipo)
                };
            }
            else
            {
               parameters = new List<SqlParameter>
                {
                    // @boleto int, @avion int, @dni int, @ciudado int, @ciudadd int, 
                    // @fp datetime, @fl datetime, @precio float, @tipo varchar(50)
                    acceso.CrearParametro("@boleto", pasaje.NumeroPasaje),
                    acceso.CrearParametro("@avion", pasaje.Avion.Patente),
                    acceso.CrearParametro("@dni", pasaje.Pasajero.DNI),
                    acceso.CrearParametro("@ciudado", ciudadDAL.GetCiudadId(pasaje._cOrigen.Nombre)),
                    acceso.CrearParametro("@ciudadd", ciudadDAL.GetCiudadId(pasaje._cDestino.Nombre)),
                    acceso.CrearParametro("@fp", pasaje.FechaOrigen),
                    acceso.CrearParametro("@fl", pasaje.FechaLlegada),
                    acceso.CrearParametro("@precio", pasaje.Precio),
                    acceso.CrearParametro("@tipo", pasaje.Tipo)
                };
            }
            return acceso.Escribir("AltaViaje", parameters, CommandType.StoredProcedure);
        }

        public int Borrar(Pasaje pasaje) ///cancela un viaje
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@boleto", pasaje.NumeroPasaje);
            return acceso.Escribir("Bajaviaje", parameter, CommandType.StoredProcedure);
        }
               
        public List<Pasaje> GetPasajes()
        {
            Acceso acceso = new Acceso();
            List<Pasaje> Pasajes = new List<Pasaje>();
            acceso.AbrirConexion();
            DataTable dataTable = acceso.LeerUno("ListarVuelos");            
            acceso.CerrarConexion();
            foreach (DataRow row in dataTable.Rows)
            {
                Pasaje pasaje = new Pasaje
                {
                    Avion = avionDAL.GetAvion(int.Parse(row[9].ToString())),
                    Tipo = row["Tipo de Viaje"].ToString(),
                    FechaLlegada = DateTime.Parse(row[11].ToString()),
                    FechaOrigen = DateTime.Parse(row[6].ToString()),
                    NumeroPasaje = int.Parse(row[0].ToString()),
                    Pasajero = pasajeroDAL.GetPasajero(int.Parse(row[1].ToString())),
                    Precio = float.Parse(row[13].ToString()),                  
                    _cOrigen = ciudadDAL.GetCiudadByName(row[4].ToString()),
                    _cDestino = ciudadDAL.GetCiudadByName(row[7].ToString())
                };
                Pasajes.Add(pasaje);
            }
            return Pasajes;
        }

        public void Depurar()
        {
            DateTime today = DateTime.Today;
            Acceso acceso = new Acceso();
            string query = "delete from Pasaje where Pasaje.FechaHoraPartida < '" +
                today.Month.ToString() + "-" + today.Day.ToString() +"-"+ today.Year.ToString() + "'";
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery(query);
            while (lector.Read()) {}
            lector.Close();
            acceso.CerrarConexion();
        }

        public List<Pasaje> FiltroDinamico(string sQL)
        {
            Acceso acceso = new Acceso();
            List<Pasaje> pasajes = new List<Pasaje>();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery(sQL  + " order by P.NumeroBoleto");
            while (lector.Read())
            {
                Pasaje pasaje = new Pasaje
                {
                    Avion = avionDAL.GetAvion(int.Parse(lector["PatenteAvion"].ToString())),
                    Tipo = lector["Tipo"].ToString(),
                    FechaLlegada = DateTime.Parse(lector["FechaHoraLlegada"].ToString()),
                    FechaOrigen = DateTime.Parse(lector["FechaHoraPartida"].ToString()),
                    NumeroPasaje = int.Parse(lector["NumeroBoleto"].ToString()),
                    Pasajero = pasajeroDAL.GetPasajero(int.Parse(lector["DNIPasajero"].ToString())),
                    Precio = float.Parse(lector["Precio"].ToString()),
                    _cOrigen = ciudadDAL.GetCiudadByName(lector["origen"].ToString()),
                    _cDestino = ciudadDAL.GetCiudadByName(lector["destino"].ToString())
                };
                pasajes.Add(pasaje);
            }
            lector.Close();
            acceso.CerrarConexion();
            return pasajes;
        }

        public int CrearNumeroDePasaje()
        {
            Acceso acceso = new Acceso();
            int num = 0; 
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery(" select ISNULL(MAX(Pasaje.NumeroBoleto),0)+1 as 'boleto' from Pasaje ");
            while (lector.Read())
            {
                num = int.Parse(lector[0].ToString());
            }
            lector.Close();
            acceso.CerrarConexion();
            return num;
        }
        public Pasaje GetPasaje(int numero)
        {
            Acceso acceso = new Acceso();
            Pasaje p = new Pasaje();
            SqlParameter parameter = acceso.CrearParametro("@boleto", numero);
            acceso.AbrirConexion();            
            DataTable dataTable = acceso.LeerUno("ObtenerVuelo", parameter);
            acceso.CerrarConexion();
            foreach (DataRow row in dataTable.Rows)
            {
                p.NumeroPasaje = numero;
                p.Avion = avionDAL.GetAvion(int.Parse(row["Avion nro"].ToString()));
                p.Tipo = row["Tipo de Viaje"].ToString();
                p.FechaLlegada = DateTime.Parse(row["FechaHoraLlegada"].ToString());
                p.FechaOrigen = DateTime.Parse(row["FechaHoraPartida"].ToString());
                p.Pasajero = pasajeroDAL.GetPasajero(int.Parse(row["DNIPasajero"].ToString()));
                p.Precio = float.Parse(row["Precio"].ToString());
                p._cOrigen = ciudadDAL.GetCiudadByName(row["Ciudad de Origen"].ToString());
                p._cDestino = ciudadDAL.GetCiudadByName(row["Ciudad de Destino"].ToString());               
            }
            return p;
        }
    }
}
