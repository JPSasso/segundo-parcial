﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class EscalaDAL
    {
        CiudadDAL ciudadDAL = new CiudadDAL();
        public int Insertar(Pasaje pasaje, Escala escala)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@boleto int, @ciudad int, @fp datetime, @fl datetime
                acceso.CrearParametro("@boleto", pasaje.NumeroPasaje),
                acceso.CrearParametro("@ciudad", ciudadDAL.GetCiudadId(escala.CiudadEscala.Nombre)),
                acceso.CrearParametro("@fp", escala.FechaOrigen),
                acceso.CrearParametro("@fl", escala.FechaLlegada)
            };
            return acceso.Escribir("AltaEscala", parameters, CommandType.StoredProcedure);
        }

        public int Borrar(Pasaje pasaje, Escala escala)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@boleto", pasaje.NumeroPasaje),
                acceso.CrearParametro("@ciudad", ciudadDAL.GetCiudadId(escala.CiudadEscala.Nombre))
            };
            return acceso.Escribir("BajaEscala", parameters, CommandType.StoredProcedure);
        }

        public int BorrarTodaslasEscalas(Pasaje pasaje)
        {
            Acceso acceso = new Acceso();
            SqlParameter sqlParameter = acceso.CrearParametro("@boleto", pasaje.NumeroPasaje);            
            return acceso.Escribir("BajaTodasEscalas", sqlParameter, CommandType.StoredProcedure);
        }

        public int Modificar(Pasaje pasaje, Escala escala, Ciudad ciudadNueva)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@boleto int, @ciudad int,@ciudad2 int, @fp datetime, @fl datetime 
                acceso.CrearParametro("@boleto", pasaje.NumeroPasaje),
                acceso.CrearParametro("@ciudad", ciudadDAL.GetCiudadId(escala.CiudadEscala.Nombre)),
                acceso.CrearParametro("@ciudad2", ciudadDAL.GetCiudadId(ciudadNueva.Nombre)),
                acceso.CrearParametro("@fp", escala.FechaOrigen),
                acceso.CrearParametro("@fl", escala.FechaLlegada)
            };
            return acceso.Escribir("ModifEscala", parameters, CommandType.StoredProcedure);
        }

        public List<Escala> GetEscalas(Pasaje pasaje)
        {
            Acceso acceso = new Acceso();
            List<Escala> Escalas = new List<Escala>();
            SqlParameter parameter = acceso.CrearParametro("@boleto", pasaje.NumeroPasaje);
            acceso.AbrirConexion();
            DataTable dataTable = acceso.LeerUno("ListarEscalas",parameter);
            acceso.CerrarConexion();
            foreach (DataRow R in dataTable.Rows)
            {
                Escala E = new Escala
                {
                    CiudadEscala = ciudadDAL.GetCiudadByName(R["Ciudad de Escala"].ToString()),
                    FechaOrigen = DateTime.Parse(R["Partida de escala"].ToString()),
                    FechaLlegada = DateTime.Parse(R["Llegada a escala"].ToString())
                };
                Escalas.Add(E);
            }
            return Escalas;
        }
        public Escala GetEscala(Pasaje pasaje, Ciudad ciudad)
        {
            Acceso acceso = new Acceso();
            Escala E = new Escala();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                acceso.CrearParametro("@boleto", pasaje.NumeroPasaje),
                acceso.CrearParametro("@ciudad",ciudadDAL.GetCiudadId(ciudad.Nombre))
            };
            acceso.AbrirConexion();
            DataTable dt = acceso.Leer("GetEscala", parameters);
            acceso.CerrarConexion();
            foreach (DataRow R in dt.Rows)
            {
                E.CiudadEscala = ciudadDAL.GetCiudadByName(R["Ciudad de Escala"].ToString());
                E.FechaOrigen = DateTime.Parse(R["Partida de escala"].ToString());
                E.FechaLlegada = DateTime.Parse(R["Llegada a escala"].ToString());
            }
            return E;
        }
    }
}
