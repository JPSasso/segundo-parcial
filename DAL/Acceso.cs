﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class Acceso
    {
        private SqlConnection connection;

        public void AbrirConexion()
        {
            connection = new SqlConnection(@"Data Source=DESKTOP-1BOSD8G;Initial Catalog=Aeropuerto;Integrated Security=True");
            if (connection != null && connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }
        public int Escribir(string SQL, List<SqlParameter> parameters = null, CommandType type = CommandType.Text)
        {
            AbrirConexion();
            int filasAfectadas = 0;
            using (SqlCommand command = CrearComando(SQL, parameters, type))
            {
                try
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        filasAfectadas = command.ExecuteNonQuery();
                        CerrarConexion();
                    }
                    else
                    {
                        CerrarConexion();
                    }
                }
                catch (SqlException)
                {
                    filasAfectadas = -1;
                }
                catch (Exception)
                {
                    filasAfectadas = -2;
                }
            }

            return filasAfectadas;
        }
        public int Escribir(string SQL, SqlParameter parameter = null, CommandType type = CommandType.Text)
        {
            AbrirConexion();
            int filasAfectadas = 0;
            using (SqlCommand command = CrearComando(SQL, parameter, type))
            {
                try
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        filasAfectadas = command.ExecuteNonQuery();
                        CerrarConexion();
                    }
                    else
                    {
                        CerrarConexion();
                    }
                }
                catch (SqlException)
                {
                    filasAfectadas = -1;
                }
                catch (Exception)
                {
                    filasAfectadas = -2;
                }
            }
            return filasAfectadas;
        }
        public SqlDataReader LeerSQLQuery(string SQLQuery)
            /// Con commandType = Text
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = connection;
            comando.CommandText = SQLQuery;
            comando.CommandType = System.Data.CommandType.Text;

            SqlDataReader lector = comando.ExecuteReader();

            return lector;
        }
        public DataTable Leer(string SQL, List<SqlParameter> parameters = null)
        {
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter
            {
                SelectCommand = CrearComando(SQL, parameters, CommandType.StoredProcedure)
            })
            {
                DataTable tabla = new DataTable();
                dataAdapter.Fill(tabla);
                return tabla;
            }
        }
        public DataTable LeerUno(string SQL, SqlParameter parameter = null)
        {
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter
            {
                SelectCommand = CrearComando(SQL, parameter, CommandType.StoredProcedure)
            })
            {
                DataTable tabla = new DataTable();
                dataAdapter.Fill(tabla);
                return tabla;
            }
        }
        public SqlCommand CrearComando(string SQL, SqlParameter parameter, CommandType type = CommandType.Text)
        {
            SqlCommand command = new SqlCommand(SQL) { CommandType = type };
            if (parameter != null)
            {
                command.Parameters.Add(parameter);
            }
            command.Connection = connection;
            return command;
        }
        public void CerrarConexion()
        {
            if (connection != null && connection.State == ConnectionState.Open)
            {
                connection.Close();
                connection.Dispose();
                GC.Collect();
                connection = null;
            }
        }
        public SqlCommand CrearComando(string SQL, List<SqlParameter> parameters, CommandType type = CommandType.Text)
        {
            SqlCommand command = new SqlCommand(SQL) { CommandType = type };
            if (parameters != null && parameters.Count > 0)
            {
                command.Parameters.AddRange(parameters.ToArray());
            }
            command.Connection = connection;
            return command;
        }
        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor)
            {
                SqlDbType = SqlDbType.VarChar
            };
            return parameter;
        }
        public SqlParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor)
            {
                SqlDbType = SqlDbType.Float
            };
            return parameter;
        }
        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor)
            {
                SqlDbType = SqlDbType.Int
            };
            return parameter;
        }
        public SqlParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor)
            {
                SqlDbType = SqlDbType.DateTime
            };
            return parameter;
        }
    }
}
