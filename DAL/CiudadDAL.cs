﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CiudadDAL
    {
        PaisDAL _PaisDAL = new PaisDAL();
        public int Insertar(Ciudad ciudad)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@nombre varchar(50), @pais int, @lat float, @long float
                acceso.CrearParametro("@nombre", ciudad.Nombre),
                acceso.CrearParametro("@pais", _PaisDAL.GetPaisId(ciudad.Pais.Nombre)),
                acceso.CrearParametro("@lat", ciudad.Latitud),
                acceso.CrearParametro("@long", ciudad.Longitud)
            };
            return acceso.Escribir("AltaCiudad", parameters, CommandType.StoredProcedure);
        }

        public int Borrar(Ciudad ciudad)
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@id", GetCiudadId(ciudad.Nombre));
            return acceso.Escribir("BajaCiudad", parameter, CommandType.StoredProcedure);
        }

        public int Modificar(Ciudad ciudad)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@id int, @nombre varchar(50), @pais int, @lat float, @long float
                acceso.CrearParametro("@id", GetCiudadId(ciudad.Nombre)),
                acceso.CrearParametro("@nombre", ciudad.Nombre),
                acceso.CrearParametro("@pais", _PaisDAL.GetPaisId(ciudad.Pais.Nombre)),
                acceso.CrearParametro("@lat", ciudad.Latitud),
                acceso.CrearParametro("@long", ciudad.Longitud)
            };
            return acceso.Escribir("ModifCiudad", parameters, CommandType.StoredProcedure);
        }

        public List<Ciudad> GetCiudades()
        {
            Acceso acceso = new Acceso();
            List<Ciudad> Ciudades = new List<Ciudad>();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select * from Ciudad");
            while (lector.Read())
            {
                Ciudad c = new Ciudad
                {
                    Nombre = lector["Nombre"].ToString(),
                    Latitud = float.Parse(lector["Latitud"].ToString()),
                    Longitud = float.Parse(lector["Longitud"].ToString()),
                    Pais = _PaisDAL.GetPaisById(int.Parse(lector["Pais"].ToString()))
                };
                Ciudades.Add(c);
            }
            lector.Close();
            acceso.CerrarConexion();            
            return Ciudades;
        }
        public Ciudad GetCiudadByName(string nombre)
        {
            Acceso acceso = new Acceso();
            Ciudad C = new Ciudad();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select * from Ciudad where Ciudad.Nombre like '%"+nombre+"%'");
            while (lector.Read())
            {
                C.Nombre = lector["Nombre"].ToString();
                C.Latitud = float.Parse(lector["Latitud"].ToString());
                C.Longitud = float.Parse(lector["Longitud"].ToString());
                C.Pais = _PaisDAL.GetPaisById(int.Parse(lector[2].ToString()));
            }
            lector.Close();
            acceso.CerrarConexion();
            return C;
        }
        public List<Ciudad> GetCiudadesByName(string nombre)
        {
            Acceso acceso = new Acceso();
            List<Ciudad> Ciudades = new List<Ciudad>();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select * from Ciudad where Ciudad.Nombre like '%" + nombre + "%'");
            while (lector.Read())
            {
                Ciudad c = new Ciudad
                {
                    Nombre = lector["Nombre"].ToString(),
                    Latitud = float.Parse(lector["Latitud"].ToString()),
                    Longitud = float.Parse(lector["Longitud"].ToString()),
                    Pais = _PaisDAL.GetPaisById(int.Parse(lector["Pais"].ToString()))
                };
                Ciudades.Add(c);
            }
            lector.Close();
            acceso.CerrarConexion();
            return Ciudades;
        }
        public int GetCiudadId(string nombre)
        {
            Acceso acceso = new Acceso();
            int P = 0;
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select Ciudad.Id from Ciudad where Ciudad.Nombre = '" + nombre +"'");
            while (lector.Read())
            {
                P = int.Parse(lector["Id"].ToString());
            }
            lector.Close();
            acceso.CerrarConexion();
            return P;
        }
        public Ciudad GetciudadById(int id)
        {
            Acceso acceso = new Acceso();
            Ciudad C = new Ciudad();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select Ciudad.Nombre from ciudad where ciudad.Id = " + id);
            while (lector.Read())
            {
                C.Nombre = lector["Nombre"].ToString();
                C.Latitud = float.Parse(lector["Latitud"].ToString());
                C.Longitud = float.Parse(lector["Longitud"].ToString());
                C.Pais = _PaisDAL.GetPaisById(int.Parse(lector["Pais"].ToString()));
            }
            lector.Close();
            acceso.CerrarConexion();
            return C;
        }
    }
}
