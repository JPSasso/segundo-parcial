﻿using BE;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class PaisDAL
    {
        public int Insertar(Pais pais)
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@nombre", pais.Nombre);           
            return acceso.Escribir("AltaPais", parameter, CommandType.StoredProcedure);
        }

        public int Borrar(Pais pais)
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@id", GetPaisId(pais.Nombre));
            return acceso.Escribir("BajaPais", parameter, CommandType.StoredProcedure);
        }

        public int Modificar(Pais pais, string nombre)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@id int, @nombre varchar(50)
                acceso.CrearParametro("@id", GetPaisId(pais.Nombre)),
                acceso.CrearParametro("@nombre", nombre)
            };
            return acceso.Escribir("ModifPais", parameters, CommandType.StoredProcedure);
        }

        public List<Pais> GetPaises()
        {
            Acceso acceso = new Acceso();
            List<Pais> paises = new List<Pais>();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select * from Pais");
            while (lector.Read())
            {
                Pais P = new Pais
                {
                    Nombre = lector["Nombre"].ToString()
                };
                paises.Add(P);
            }
            lector.Close();
            acceso.CerrarConexion();
            return paises;
        }
              
        public int GetPaisId(string nombre)
        {
            Acceso acceso = new Acceso();
            int P = 0;
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select Pais.Id from Pais where Pais.Nombre = '" + nombre + "'");
            while (lector.Read())
            {
                P = int.Parse(lector["Id"].ToString());
            }
            lector.Close();
            acceso.CerrarConexion();            
            return P;
        }
        public Pais GetPaisById(int id)
        {
            Acceso acceso = new Acceso();
            Pais P = new Pais();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select Pais.Nombre from Pais where Pais.Id = " + id);
            while (lector.Read())
            {
                P.Nombre = lector[0].ToString();
            }
            lector.Close();
            acceso.CerrarConexion();
            return P;
        }
        public Pais GetPaisByName(string nombre)
        {
            Acceso acceso = new Acceso();
            Pais P = new Pais();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("select Pais.Id from Pais where Pais.Nombre like '%" + nombre +"%'");
            while (lector.Read())
            {
                P.Nombre = lector["Nombre"].ToString();
            }
            lector.Close();
            acceso.CerrarConexion();
            return P;
        }
        public void SetCities (Pais pais)
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@nombre", pais.Nombre);
            acceso.AbrirConexion();
            DataTable table = acceso.LeerUno("ListarCiudadesDePais", parameter); 
            acceso.CerrarConexion();
            foreach (DataRow row in table.Rows)            
            {
                Ciudad c = new Ciudad
                {
                    Nombre = row["Nombre"].ToString(),
                    Latitud = float.Parse(row["Latitud"].ToString()),
                    Longitud = float.Parse(row["Longitud"].ToString()),
                    Pais = pais
                };
                pais.Ciudades.Add(c);
            }           
        }
    }
}