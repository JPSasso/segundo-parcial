﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PasajeroDAL
    {
        public int Insertar(Pasajero pasajero)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@dni int, @nombre varchar(50), @apellido varchar(50)
                acceso.CrearParametro("@dni", pasajero.DNI),
                acceso.CrearParametro("@nombre", pasajero.Nombre),
                acceso.CrearParametro("@apellido", pasajero.Apellido)
            };
            return acceso.Escribir("AltaPasajero", parameters, CommandType.StoredProcedure);
        }

        public int Borrar(Pasajero pasajero)
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@dni", pasajero.DNI);
            return acceso.Escribir("BajaPasajero", parameter, CommandType.StoredProcedure);
        }

        public int Modificar(Pasajero pasajero)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@dni int, @nombre varchar(50), @apellido varchar(50)
                acceso.CrearParametro("@dni", pasajero.DNI),
                acceso.CrearParametro("@nombre", pasajero.Nombre),
                acceso.CrearParametro("@apellido", pasajero.Apellido)
            };
            return acceso.Escribir("ModifPasajero", parameters, CommandType.StoredProcedure);
        }

        public List<Pasajero> GetPasajeros()
        {
            Acceso acceso = new Acceso();
            List<Pasajero> Pasajeros = new List<Pasajero>();
            acceso.AbrirConexion();
            DataTable dataTable = acceso.Leer("ListarPasajeros");
            acceso.CerrarConexion();
            foreach (DataRow R in dataTable.Rows)
            {
                Pasajero P = new Pasajero
                {
                    /*[DNI]
                     * ,[Nombre]
                      ,[Apellido]
                      */
                    DNI = int.Parse(R["DNI"].ToString()),
                    Nombre = R["Nombre"].ToString(),
                    Apellido = R["Apellido"].ToString()

                };
                Pasajeros.Add(P);
            }
            return Pasajeros;
        }
        public Pasajero GetPasajero(int dni)
        {
            Acceso acceso = new Acceso();
            Pasajero P = new Pasajero();
            SqlParameter parameter = acceso.CrearParametro("@dni", dni);
            acceso.AbrirConexion();
            DataTable dt = acceso.LeerUno("GetPasajero", parameter);
            acceso.CerrarConexion();
            foreach (DataRow R in dt.Rows)
            {
                P.DNI = int.Parse(R["DNI"].ToString());
                P.Nombre = R["Nombre"].ToString();
                P.Apellido = R["Apellido"].ToString();
            }
            return P;
        }
        public List<Pasajero> GetPasajerosByDNI(int dni)
        {
            Acceso acceso = new Acceso();
            List<Pasajero> pasajeros = new List<Pasajero>();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery(" select * from Pasajero where Pasajero.DNI Like '" + dni + "%'");
            while (lector.Read())
            {
                Pasajero P = new Pasajero {
                    DNI = int.Parse(lector["DNI"].ToString()),
                    Nombre = lector["Nombre"].ToString(),
                    Apellido = lector["Apellido"].ToString()
                };
                pasajeros.Add(P);
            }
            lector.Close();
            acceso.CerrarConexion();
            return pasajeros;
        }

    }
}
