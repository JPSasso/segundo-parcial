﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AvionDAL
    {
        public int Insertar(Avion avion)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@patente int, @modelo varchar(50), @capa int
                acceso.CrearParametro("@patente", avion.Patente),
                acceso.CrearParametro("@modelo", avion.Modelo),
                acceso.CrearParametro("@capa", avion.Capacidad),
                acceso.CrearParametro("@ocu", avion.Ocupacion),
                acceso.CrearParametro("@pasaj", avion.Pasajeros.Count)
            };
            return acceso.Escribir("AltaAvion", parameters, CommandType.StoredProcedure);
        }

        public Avion GetAvion(string nombre)
        {
            Acceso acceso = new Acceso();
            Avion avion = new Avion();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("SELECT * FROM [Aeropuerto].[dbo].[Avion] where Avion.Modelo = '" + nombre +"'");
            while (lector.Read())
            {
                avion.Modelo = nombre;
                avion.Patente = int.Parse(lector["Patente"].ToString());
                avion.Capacidad = int.Parse(lector["Capacidad"].ToString());
                avion.Ocupacion = int.Parse(lector["Ocupacion"].ToString());
                avion.Pasajeros = GetPasajeros(avion);
            }
            lector.Close();
            acceso.CerrarConexion();
            return avion;
        }

        public int Borrar(Avion avion)
        {
            Acceso acceso = new Acceso();
            SqlParameter parameter = acceso.CrearParametro("@patente", avion.Patente);
            return acceso.Escribir("BajaAvion", parameter, CommandType.StoredProcedure);
        }

        public int Modificar(Avion avion)
        {
            Acceso acceso = new Acceso();
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                //@patente int, @modelo varchar(50), @capa int, @ocu int
                acceso.CrearParametro("@patente", avion.Patente),
                acceso.CrearParametro("@modelo", avion.Modelo),
                acceso.CrearParametro("@capa", avion.Capacidad),
                acceso.CrearParametro("@ocu", avion.Ocupacion),
                acceso.CrearParametro("@pasaj", avion.Pasajeros.Count)
            };
            return acceso.Escribir("ModifAvion", parameters, CommandType.StoredProcedure);
        }
        public List<Avion> GetAviones()
        {
            Acceso acceso = new Acceso();
            List<Avion> Aviones = new List<Avion>();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("SELECT * FROM [Aeropuerto].[dbo].[Avion]");
            while (lector.Read())
            {
                //Obteniendo los campos
                Avion avion = new Avion
                {
                    Patente = int.Parse(lector["Patente"].ToString()),
                    Modelo = lector["Modelo"].ToString(),
                    Capacidad = int.Parse(lector["Capacidad"].ToString()),
                    Ocupacion = int.Parse(lector["Ocupacion"].ToString())
                };
                avion.Pasajeros = GetPasajeros(avion);
                Aviones.Add(avion);
            }            
            lector.Close();
            acceso.CerrarConexion();
            return Aviones;
        }
        public Avion GetAvion(int patente)
        {
            Acceso acceso = new Acceso();
            Avion avion = new Avion();
            acceso.AbrirConexion();
            SqlDataReader lector = acceso.LeerSQLQuery("SELECT * FROM [Aeropuerto].[dbo].[Avion] where Avion.Patente = "+ patente);
            while (lector.Read())
            {
                avion.Patente = patente;
                avion.Modelo = lector["Modelo"].ToString();
                avion.Capacidad = int.Parse(lector["Capacidad"].ToString());
            }
            lector.Close();
            acceso.CerrarConexion();
            return avion;
        }

        public List<Pasajero> GetPasajeros (Avion avion)
        {
            List<Pasajero> pasajeros = new List<Pasajero>();
            Acceso acceso = new Acceso();
            SqlParameter sqlParameter = acceso.CrearParametro("avion", avion.Patente);
            acceso.AbrirConexion();
            DataTable dataTable = acceso.LeerUno("ListarPasajerosXavion", sqlParameter);
            acceso.CerrarConexion();
            foreach (DataRow row in dataTable.Rows)
            {
                Pasajero P = new Pasajero
                {
                    DNI = int.Parse(row["DNI"].ToString()),
                    Nombre = row["Nombre"].ToString(),
                    Apellido = row["Apellido"].ToString()
                };
                pasajeros.Add(P);
            }
            avion.Pasajeros = pasajeros;
            return pasajeros;
        }

    }
}
